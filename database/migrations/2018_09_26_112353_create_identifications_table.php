<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdentificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('identifications', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id');
        $table->integer('status'); // 0 - not provided | 1 = pending verification | 2 = verified | 3 = verification issue | 4 = TBD
        $table->integer('type'); // 0 + request | 1 = provided | 2 = TBD
        $table->string('id_name');
        $table->longText('note'); // if there's an issue
        $table->string('filename');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identifications');
    }
}
