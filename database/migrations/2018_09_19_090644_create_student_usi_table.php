<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentUsiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_usi', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->string('usi_number');
          $table->integer('status'); // 0 = not supplied | 1 = pending | 2 = verified | 3 = TBD | 4 = verification issue
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_usi');
    }
}
