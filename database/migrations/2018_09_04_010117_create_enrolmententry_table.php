<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrolmententryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolmententry', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status'); // if approved it will create a user in the LMS and portal
            $table->string('firstname');
            $table->string('lastname');
            $table->date('dateofbirth');
            $table->integer('sex');
            $table->boolean('usi');
            $table->string('usinumber');
            $table->boolean('usiagree');
            // Residential Address
            $table->string('street_address');
            $table->string('city');
            $table->string('state');
            $table->integer('postal_code');
            $table->string('country');
            // Postal Address
            $table->boolean('postal_same');
            $table->string('postal_street_address');
            $table->string('postal_city');
            $table->string('postal_state');
            $table->integer('postal_postal_code');
            $table->string('postal_country');
            // Contact Information
            $table->integer('home_number');
            $table->integer('mobile_number');
            $table->string('email');
            // Course Details
            $table->string('course_category');
            $table->string('course_name');
            $table->boolean('rpl');
            $table->string('country_born');
            $table->string('language');
            $table->integer('english');
            $table->integer('aboriginal');
            $table->boolean('disability');
            $table->integer('disabilities'); // array of numbers
            $table->integer('school_level');
            $table->integer('year_school_level');
            $table->boolean('still_attending_school');
            // Prior Qualification
            $table->boolean('prior_qualification');
            $table->integer('qualifications'); // array of numbers
            // Employment
            $table->integer('employment_status');
            $table->integer('study_reason');
            // Company Details
            $table->string('employer_title')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_street')->nullable();
            $table->string('company_city')->nullable();
            $table->string('company_state')->nullable();
            $table->integer('company_postal_code')->nullable();
            $table->string('work_phone_number')->nullable();
            $table->string('work_mobile_number')->nullable();
            $table->string('work_email')->nullable();
            // First Aid
            $table->boolean('first_aid_cert');
            // Agreement
            $table->boolean('photo_emailed');
            $table->integer('identification'); // array of numbers
            $table->boolean('cert_security');
            $table->boolean('security_license');
            $table->integer('documents'); // array of numbers
            $table->string('fullname_declaration');
            $table->boolean('certify_agreement');
            $table->date('dateofenrolment');
            $table->string('discovered');
            $table->ipAddress('ip_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrolmententry');
    }
}
