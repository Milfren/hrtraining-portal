<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_list', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('categoryid');
          $table->string('code');
          $table->string('title');
          $table->string('slug');
          $table->longText('description');
          $table->boolean('visible');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_list');
    }
}
