<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntroCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intro_courses', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('lms_courseid');
          $table->integer('user_id');
          $table->string('grade');
          $table->string('fullname');
          $table->integer('type'); // 0 = LLN | 1 = Induction
          $table->integer('status');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intro_courses');
    }
}
