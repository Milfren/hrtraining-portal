<?php

use Illuminate\Database\Seeder;

class UnitSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('unit_section')->insert([
        [
          'courseid' => 1,
          'title' => '3 Core Units',
          'description' => ''
        ],
        [
          'courseid' => 1,
          'title' => 'Eight (8) ELECTIVE Units',
          'description' => 'Choose a minimum of six (6) units, up to a maximum of eight (8) units from the list below'
        ],
        [
          'courseid' => 1,
          'title' => 'Choose up to two (2) Elective Units from Certificate III CPP30316',
          'description' => 'If you choose less than 8 units from above, choose up to two (2) CPP30316 units'
        ]
      ]);
    }
}
