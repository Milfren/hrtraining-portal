<?php

use Illuminate\Database\Seeder;

class UnitListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unit_list')->insert([
          [
            'courseid' => 1,
            'sectionid' => 1,
            'code' => 'CPPCLO2032',
            'title' => 'Plan basic cleaning activities',
            'note' => '',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 1,
            'code' => 'CPPCLO2034',
            'title' => 'Maintain storage area and cleaning equipment',
            'note' => '',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 1,
            'code' => 'CPPCMN2002',
            'title' => 'Participate in workplace safety arrangements',
            'note' => '',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCCL2008',
            'title' => 'Clean carpeted floors',
            'note' => '(includes practical assessment)',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCCL2009',
            'title' => 'Perform basic stain removal from carpets',
            'note' => '(includes practical assessment)',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCLO2002',
            'title' => 'Clean hard floor surfaces',
            'note' => '(includes practical assessment)',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCLO2005',
            'title' => 'Maintain Glass Surfaces',
            'note' => '(includes practical assessment)',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCLO2014',
            'title' => 'Clean and arrange furniture and fittings',
            'note' => '',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCLO2016',
            'title' => 'Clean wet surfaces',
            'note' => '(includes practical assessment)',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCLO2018',
            'title' => 'Sort, remove and recycle waste material',
            'note' => '',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCLO2042',
            'title' => 'Clean surfaces using microfibre equipment',
            'note' => '',
            'visible' => true,
          ],
          [
            'courseid' => 1,
            'sectionid' => 2,
            'code' => 'CPPCMN2004',
            'title' => 'Provide basic client services',
            'note' => '',
            'visible' => true,
          ]
        ]
      );
    }
}
