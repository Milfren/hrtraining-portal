<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('client')->insert([
        [
          'name' => 'Education Queensland',
          'description' => null,
          'folder' => 'clients'.str_random(10).'1'
        ],
        [
          'name' => 'Gold Coast City Council',
          'description' => 'This is a gold coast city council description',
          'folder' => 'clients'.str_random(10).'2'
        ],
        [
          'name' => 'Qantas Airline',
          'description' => 'This is a Qantas Airline description',
          'folder' => 'clients'.str_random(10).'3'
        ]
      ]);
    }
}
