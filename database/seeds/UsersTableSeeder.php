<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        [
          'lms_user_id' => 0,
          'first_name' => 'Milfren John',
          'last_name' => 'Admin',
          'username' => 'admin',
          'email' => 'milfren@hrtraining.com.au',
          'password' => bcrypt('Homest3ad'),
          'folder' => '1D' . str_random(15)
        ],
        [
          'lms_user_id' => 0,
          'first_name' => 'Student',
          'last_name' => 'Role',
          'username' => 'student.role',
          'email' => 'student@hrtraining.com.au',
          'password' => bcrypt('Homest3ad'),
          'folder' => '2D' . str_random(15)
        ],
        [
          'lms_user_id' => 0,
          'first_name' => 'Jhun',
          'last_name' => 'Acain',
          'username' => 'jhun.portal',
          'email' => 'j.acain@hrtraining.com.au',
          'password' => bcrypt('Homest3ad'),
          'folder' => '3D' . str_random(15)
        ],
        [
          'lms_user_id' => 0,
          'first_name' => 'Garry',
          'last_name' => 'Taylor',
          'username' => 'garry.portal',
          'email' => 'g.taylor@hrtraining.com.au',
          'password' => bcrypt('Homest3ad'),
          'folder' => '4D' . str_random(15)
        ]

      ]);
    }
}
