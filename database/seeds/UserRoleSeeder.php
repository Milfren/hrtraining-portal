<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('user_role')->insert([
        [
          'user_id' => 1,
          'role' => 1
        ],
        [
          'user_id' => 2,
          'role' => 3
        ],
        [
          'user_id' => 3,
          'role' => 1
        ],
        [
          'user_id' => 4,
          'role' => 1
        ]

      ]);
    }
}
