<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(UsersTableSeeder::class);
      $this->call(UnitListTableSeeder::class);
      $this->call(UnitSectionSeeder::class);
      $this->call(CourseCategorySeeder::class);
      $this->call(CourseListSeeder::class);
      $this->call(UserRoleSeeder::class);
      $this->call(ClientTableSeeder::class);
    }
}
