<?php

use Illuminate\Database\Seeder;

class CourseCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('course_category')->insert([
        [
          'title' => 'Cleaning Operations and Cleaning Management',
          'short_name' => 'cleaning',
          'slug' => str_slug('Cleaning Operations and Cleaning Management', '-'),
          'description' => '<p>HR training offers a range of cleaning certificates in both operations and management, using The Professional Cleaners Handbook as the learning resource.</p><p>You are able to complete our cleaning operations courses online or face-to-face.</p><p>Training is customised to the needs of each organisation and/or individual person who enrols with us to ensure the best results and outcomes for each client. Discuss with us your particular situation and your needs; with over 24 years’ experience in vocational training in the workplace, we can give clients options and advice based on your goals…it’s hard to know what you want – when you don’t understand all that’s available.</p>'
        ],
        [
          'title' => 'Business and Management',
          'short_name' => 'business',
          'slug' => str_slug('Business and Management', '-'),
          'description' => 'Training is customised to the needs of each organisation and/or individual person who enrols to ensure the best results and outcomes for each client. Discuss with us your situation and your needs. With over 24 years’ experience in vocational training in the workplace, we will give you options and advice on the best course and units to choose, based on your goals.'
        ],
        [
          'title' => 'Security and Risk Management',
          'short_name' => 'security',
          'slug' => str_slug('Security and Risk Management', '-'),
          'description' => '<p>HR training offers a range of cleaning certificates in both operations and management, using The Professional Cleaners Handbook as the learning resource.</p><p>You are able to complete our cleaning operations courses online or face-to-face.</p><p>Training is customised to the needs of each organisation and/or individual person who enrols with us to ensure the best results and outcomes for each client. Discuss with us your particular situation and your needs; with over 24 years’ experience in vocational training in the workplace, we can give clients options and advice based on your goals…it’s hard to know what you want – when you don’t understand all that’s available.</p>'
        ]
      ]);
    }
}
