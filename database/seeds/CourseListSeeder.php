<?php

use Illuminate\Database\Seeder;

class CourseListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('course_list')->insert([
        [
          'categoryid' => '1',
          'code' => 'CPP20617',
          'title' => 'Certificate II in Cleaning',
          'slug' => str_slug('Certificate II in Cleaning', '-'),
          'description' => 'This qualification is aimed at individuals who perform a range of routine cleaning tasks using practical cleaning skills and knowledge.',
          'visible' => 1
        ],
        [
          'categoryid' => '1',
          'code' => 'CPP30316',
          'title' => 'Certificate III in Cleaning Operations',
          'slug' => str_slug('Certificate III in Cleaning Operations', '-'),
          'description' => 'This NEW course was released in May 2016 and is aimed at individuals who have been cleaning for a few years and want to increase their skills and knowledge.',
          'visible' => 1
        ],
        [
          'categoryid' => '1',
          'code' => 'CPP40416',
          'title' => 'Certificate IV in Cleaning Management',
          'slug' => str_slug('Certificate IV in Cleaning Management', '-'),
          'description' => 'This NEW course was released in May 2016 and is aimed at individuals who have been cleaning for a number of years and want to increase their management skills and knowledge.',
          'visible' => 1
        ]
      ]);
    }
}
