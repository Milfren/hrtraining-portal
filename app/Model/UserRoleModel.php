<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRoleModel extends Model
{
  protected $table = 'user_role';

  protected $fillable = ['role'];

  protected $hidden = [
      'created_at', 'id', 'updated_at', 'user_id'
  ];

  public function user() {
    return $this->belongsTo('App\User');
  }
}
