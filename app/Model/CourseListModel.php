<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class CourseListModel extends Model
{
  protected $table = 'course_list';

  public function sections() {
    return $this->hasMany('App\Model\UnitSectionModel', 'courseid');
  }
}
