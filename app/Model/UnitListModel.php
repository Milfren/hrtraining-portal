<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UnitListModel extends Model
{
  protected $table = 'unit_list';
}
