<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EnrolmentModel extends Model
{
    protected $table = 'enrolmententry';

    public function statusLog() {
      return $this->hasOne('App\Model\EnrolmentStatusLogModel', 'entryid');
    }
}
