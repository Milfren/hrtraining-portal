<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentUSIModel extends Model
{
  protected $table = 'student_usi';

  protected $fillable = [
      'status'
  ];

  protected $hidden = [
      'created_at', 'updated_at',
  ];

  public function user() {
    return $this->belongsTo('App\User');
  }
}
