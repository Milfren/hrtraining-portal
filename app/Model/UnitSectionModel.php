<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UnitSectionModel extends Model
{
  protected $table = 'unit_section';

  public function units() {
    return $this->hasMany('App\Model\UnitListModel', 'sectionid');
  }
}
