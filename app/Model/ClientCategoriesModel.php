<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientCategoriesModel extends Model
{
  protected $table = 'client_categories';

  protected $hidden = [
      'created_at', 'updated_at'
  ];

  public function client() {
    return $this->belongsTo('App\Model\ClientModel', 'client_id');
  }
  public function students() {
    return $this->hasMany('App\Model\ClientCategoriesStudentModel', 'category_id');
  }
}
