<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FormNotificationsModel extends Model
{
  protected $table = 'form_notifications';
}
