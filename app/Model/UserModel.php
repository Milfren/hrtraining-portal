<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = 'users';

    public function roles()
   {
       return $this->hasMany('App\Model\UserRoleModel', 'user_id');
   }
}
