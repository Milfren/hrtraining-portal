<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IntroCoursesModel extends Model
{
  protected $table = 'intro_courses';

  protected $fillable = ['lms_courseid', 'fullname', 'type', 'grade', 'user_id', 'status'];
}
