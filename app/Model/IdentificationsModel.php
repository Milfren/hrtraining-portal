<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IdentificationsModel extends Model
{
  protected $table = 'identifications';

  public function user() {
    return $this->belongsTo('App\User');
  }
}
