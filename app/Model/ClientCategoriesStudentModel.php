<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientCategoriesStudentModel extends Model
{
  protected $table = 'client_categories_student';

  protected $hidden = [
      'created_at', 'updated_at'
  ];

  public function categories() {
    return $this->belongsTo('App\Model\ClientCategoriesModel', 'category_id');
  }
  public function user() {
    return $this->belongsTo('App\User', 'user_id');
  }
  public function client_students() {
    return $this->belongsTo('App\User', 'client_students_id');
  }
}
