<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientFilesModel extends Model
{
  protected $table = 'client_files';

  public function client() {
    return $this->belongsTo('App\Model\ClientModel', 'client_id');
  }

  public function uploaded_by() {
    return $this->belongsTo('App\User', 'uploaded_by');
  }

  public function updated_by() {
    return $this->belongsTo('App\User', 'updated_by');
  }
}
