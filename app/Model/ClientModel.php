<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientModel extends Model
{
  protected $table = 'client';

  protected $hidden = [
      'created_at', 'updated_at'
  ];

  public function students() {
    return $this->hasMany('App\Model\ClientStudentsModel', 'client_id');
  }

  public function categories() {
    return $this->hasMany('App\Model\ClientCategoriesModel', 'client_id');
  }

  public function files() {
    return $this->hasMany('App\Model\ClientFilesModel', 'client_id');
  }
}
