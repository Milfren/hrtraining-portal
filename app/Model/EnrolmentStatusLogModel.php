<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EnrolmentStatusLogModel extends Model
{
    protected $table = 'enrolment_status_log';

    public function enrolment() {
      return $this->belongsTo('App\Model\EnrolmentModel', 'entryid');
    }

    public function user() {
      return $this->belongsTo('App\User', 'userid');
    }

}
