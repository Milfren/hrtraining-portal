<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPaymentsModel extends Model
{
    protected $table = 'user_payments';
}
