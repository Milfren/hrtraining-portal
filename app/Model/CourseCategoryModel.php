<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CourseCategoryModel extends Model
{
  protected $table = 'course_category';

  public function courses() {
    return $this->hasMany('App\Model\CourseListModel', 'categoryid');
  }
}
