<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientStudentsModel extends Model
{
  protected $table = 'client_students';

  public function user() {
    return $this->belongsTo('App\User', 'user_id');
  }

  public function client() {
    return $this->belongsTo('App\Model\ClientModel', 'client_id');
  }
}
