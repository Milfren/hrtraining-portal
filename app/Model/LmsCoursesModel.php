<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LmsCoursesModel extends Model
{
  protected $table = 'lms_courses';

  protected $fillable = ['lms_courseid', 'fullname', 'grade', 'user_id', 'status'];
}
