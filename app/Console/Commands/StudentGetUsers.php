<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Model\UserRoleModel as Role;
use App\Model\StudentUSIModel as Usi;
use App\Model\IdentificationsModel as Identification;

class StudentGetUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'student:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all the student Login from LMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $users = DB::connection('mysql_lms')->table('mdl_user')->get();

      foreach ($users as $user) {
        if (!$user->deleted) {
          $create = new User;
          $create->first_name = $user->firstname;
          $create->last_name = $user->lastname;
          $create->email = $user->email;
          $create->username = $user->username;
          $create->password = $user->password;
          $create->lms_user_id = $user->id;
          $create->save();
          $create->folder = $create->id . 'D' . str_random(15);
          $create->save();

          $roleOld = new Role;
          $roleOld->user_id = $create->id;
          $roleOld->role = 5; // 5 = Old Student Role
          $roleOld->save();

          $usi = new Usi;
          $usi->user_id = $create->id;
          $usi->status = 3; // default by 3 (TBD) for old students
          $usi->save();

          $this->createId($create->id);
        }
      }
    }
    private function createId($id) {
      $id1 = new Identification;
      $id1->user_id = $id;
      $id1->status = 4; // TBD
      $id1->type = 2; // TBD
      $id1->id_name = 'Valid ID';
      $id1->note = 'Driver\'s licence (Front and Back) / Passport';
      $id1->save();

      $id2 = new Identification;
      $id2->user_id = $id;
      $id2->status = 4; // TBD
      $id2->type = 2; // TBD
      $id2->id_name = 'Head and Shoulders Photo';
      $id2->save();
    }
}
