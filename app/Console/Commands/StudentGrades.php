<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Portal\PortalEnrolmentController;
use App\Model\IntroCoursesModel as IntroCourses;
use App\Model\LmsCoursesModel as LMSCourses;
use App\User;

class StudentGrades extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'student:grades';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates the student grades from LMS to the portal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      // PUT THIS FUNCTION IN THE CRON
      $domain = 'https://hrtrainingcollege.com';
      $curl = new PortalEnrolmentController;

      $users = User::all();

      foreach ($users as $user) {
        // get the grade of user of the courses he/she is enrolled to
        $grades = json_decode($curl->callCurl('gradereport_overview_get_course_grades', 'json', 'userid='.$user->lms_user_id, $domain));

        // get the course details of the courses where the user is enrolled to
        // hidden and suspended courses are not displayed
        $courses = json_decode($curl->callCurl('core_enrol_get_users_courses', 'json', 'userid='.$user->lms_user_id, $domain));

        // add conditional statement if grade is the same
        // if grade is not the same then update
        for ($i=0; $i < count($courses); $i++) {
          if ($courses[$i]->visible) {
            for ($j=0; $j < count($grades->grades); $j++) {
              if ($grades->grades[$j]->courseid == $courses[$i]->id){
                if (strpos($courses[$i]->fullname, 'Induction')) {
                  $status = $this->getStatus($grades->grades[$j]->grade);

                  $induction = IntroCourses::updateOrCreate(
                    ['lms_courseid' => $courses[$i]->id, 'user_id' => $user->id],
                    ['fullname' => 'Induction Course', 'type' => 1, 'grade' => $grades->grades[$j]->grade, 'status' => $status]
                  );
                }
                elseif (strpos($courses[$i]->fullname, 'LLN')) {
                  $status = $this->getStatus($grades->grades[$j]->grade, true);

                  $lln = IntroCourses::updateOrCreate(
                    ['lms_courseid' => $courses[$i]->id, 'user_id' => $user->id],
                    ['fullname' => 'LLN Review', 'type' => 0, 'grade' => $grades->grades[$j]->grade, 'status' => $status]
                  );
                }
                else {
                  $status = $this->getStatus($grades->grades[$j]->grade);

                  $otherCourses = LMSCourses::updateOrCreate(
                    ['lms_courseid' => $courses[$i]->id, 'user_id' => $user->id],
                    ['fullname' => $courses[$i]->fullname, 'grade' => $grades->grades[$j]->grade, 'status' => $status]
                  );
                }
              }
            }
          }
        }
      }
    }
    private function getStatus($grade, $lln = false) {
      if ($lln) {
        if ($grade == '-' || $grade == '0.00 %') {
          return 0; // not started or not yet graded
        }
        else {
          return 2;
        }
      }
      else {
        if ($grade == '100.00 %') {
          return 2; // done
        }
        elseif ($grade == '-' || $grade == '0.00 %') {
          return 0; // not started or not yet graded
        }
        else {
          return 1; // in progress
        }
      }
    }
}
