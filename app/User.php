<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function roles() {
      return $this->hasOne('App\Model\UserRoleModel', 'user_id');
    }
    public function enrolment() {
      return $this->hasMany('App\Model\EnrolmentStatusLogModel', 'student_id');
    }
    public function payments() {
      return $this->hasMany('App\Model\UserPaymentsModel', 'user_id');
    }
    public function introCourses() {
      return $this->hasMany('App\Model\IntroCoursesModel', 'user_id');
    }
    public function courses() {
      return $this->hasMany('App\Model\LmsCoursesModel', 'user_id');
    }
    public function identifications() {
      return $this->hasMany('App\Model\IdentificationsModel', 'user_id');
    }
    public function usi() {
      return $this->hasOne('App\Model\StudentUSIModel', 'user_id');
    }
}
