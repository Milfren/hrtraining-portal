<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\UnitSectionModel;
use App\Model\CourseCategoryModel;
use App\Model\CourseListModel;

use Auth;

class CourseController extends Controller
{
    public function category() {
      $getCategory = CourseCategoryModel::all();

      return view('portal.course.category', compact('getCategory'));
    }
    public function list($slug) {
      $getCategory = CourseCategoryModel::where('slug', $slug)->first();
      $getCourses = CourseListModel::where('categoryid', $getCategory->id)->get();

      return view('portal.course.list', compact('getCategory', 'getCourses'));
    }
    public function name() {
      return view('portal.course.name');
    }
    public function edit() {
      return view('portal.course.name.edit');
    }
    public function students() {
      return view('portal.course.name.students');
    }
    public function studentsSub() {
      return view('portal.course.studentsSub');
    }
    public function offer() {
      return view('portal.course.offer');
    }
    public function units($categorySlug, $courseSlug) {
      $getCategory = CourseCategoryModel::where('slug', $categorySlug)->first();
      $getCourse = CourseListModel::where('slug', $courseSlug)->first();

      return view('portal.course.name.units', compact('getCourse'));
    }
    public function website() {
      return view('portal.course.name.website');
    }
}
