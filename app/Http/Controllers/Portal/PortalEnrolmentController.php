<?php

namespace App\Http\Controllers\Portal;

use App\User;
use App\Model\EnrolmentModel;
use App\Model\EnrolmentStatusLogModel;
use App\Model\UserModel;
use App\Model\UserRoleModel;
use App\Model\UserPaymentsModel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\cURLController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeEnrolment;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class PortalEnrolmentController extends Controller
{
  public function list() {
    $enrolments = DB::table('enrolmententry')->get();

    return view('portal.enrolment.list', ['enrolments' => $enrolments]);
  }
  public function entry($id) {
    $entry = DB::table('enrolmententry')->where('id', $id)->first();
    if ($entry->status) {
      $confirmedBy = EnrolmentStatusLogModel::where('entryid', $entry->id)->first();
      $confirmedBy->first_name = User::where('id', $confirmedBy->userid)->first()->first_name;
      $confirmedBy->last_name = User::where('id', $confirmedBy->userid)->first()->last_name;
    }
    $entryId = $id;
    $password = $this->generate_password($entry);
    return view('portal.enrolment.entry', compact('entryId', 'entry', 'confirmedBy', 'password'));
  }

  public function entryConfirm($id, Request $request) {
    // get the enrolment entry
    $entry = DB::table('enrolmententry')->where('id', $id)->first();

    $confirm = EnrolmentModel::find($id);
    $confirm->status = 1;
    $confirm->save();

    // initialize the enrolment confirmation
    // this should be initialized first
    // $this->initialStudentSetup($entry);

    // send a welcome email
    Mail::to($entry->email)->send(new WelcomeEnrolment($entry));

    $request->user_id = $this->createPortalStudentUser($entry);
    $this->createPayment($request);

    $confirmedBy = new EnrolmentStatusLogModel;
    $confirmedBy->entryid = $id;
    $confirmedBy->userid = Auth::user()->id;
    $confirmedBy->student_id = $request->user_id;
    $confirmedBy->status = 1;
    $confirmedBy->save();

    return redirect()->route('enrolment.entry', $id);
  }

  public function emailTest() {
    // return Mail::to('milfren@hrtraining.ph')->send(new WelcomeEnrolment);
    return new WelcomeEnrolment();
  }

  public function callCurl($functionName, $restformat,  $params, $url = null) {
    header('Content-Type: text/plain');
    if ($url == null) {
      $serverurl = env('LMS_DOMAIN') . '/webservice/rest/server.php'. '?wstoken=' . env('LMS_TOKEN') . '&wsfunction='.$functionName;
    }
    else {
      $serverurl = $url . '/webservice/rest/server.php'. '?wstoken=bddfb9cfbf90cb85e6862e4eecf23b65&wsfunction='.$functionName;
    }

    $curl = new cURLController;
    //if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
    $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
    return $resp = $curl->post($serverurl . $restformat, $params);
  }

  public function generate_username($first, $last) {
    $firstname = str_replace(' ', '', $first);
    $lastname = str_replace(' ', '', $last);
    $firstnamelower = strtolower($firstname);
    $lastnamelower = strtolower($lastname);
    $username = $firstnamelower.'.'.$lastnamelower;

    return $username;
  }

  public function generate_password($entry) {
    $firstChar = strtolower(substr($entry->firstname, 0, 1));
    $secondChar = strtoupper(substr($entry->lastname, 0, 1));
    $password = $firstChar.$secondChar.'.'.date('dmy', strtotime($entry->dateofbirth));

    return $password;
  }

  // This will create the user in the LMS
  // author: Milfren John dela Vega
  public function core_user_create_users($entry) {
    $functionName = 'core_user_create_users';
    $username = $this->generate_username($entry->firstname, $entry->lastname);
    $password = $this->generate_password($entry);

    $user = new \stdClass();
    $user->username = $username;
    // change password
    $user->password = $password;
    $user->firstname = $entry->firstname;
    $user->lastname = $entry->lastname;
    // change email
    $user->email = $username.'@'.$username.'.com';
    $users = array($user);
    $params = array('users' => $users);

    return $this->callCurl($functionName, 'xml', $params);
  }

  // This will get the newly enrolled student
  // author: Milfren John dela Vega
  public function core_user_get_users($entry) {
    $functionName = 'core_user_get_users';
    $username = $this->generate_username($entry->firstname, $entry->lastname);

    $getUser = new \stdClass();
    $getUser->key = 'username';
    $getUser->value = $username;
    $getUsers = array($getUser);
    $params = array('criteria' => $getUsers);

    $resp = $this->callCurl($functionName, 'json', $params);
    return json_decode($resp);
  }

  // This will enrol the student to the initial courses
  // LLN Course and HRtraining Induction Course
  // author: Milfren John dela Vega
  public function enrol_manual_enrol_users($entry) {
    $functionName = 'enrol_manual_enrol_users';

    $enrolUser = new \stdClass();
    $enrolUser->roleid = '5';
    $enrolUser->userid = $entry;
    $enrolUser->courseid = '2'; // env
    $enrolUsers = array($enrolUser);
    $params = array('enrolments' => $enrolUsers);

    return $this->callCurl($functionName, 'xml', $params);
  }

  public function initialStudentSetup($entry) {
    $this->core_user_create_users($entry);
    $getEnrolledUser = $this->core_user_get_users($entry);
    $this->enrol_manual_enrol_users($getEnrolledUser->users[0]->id);
  }
}
