<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Http\Controllers\Portal\PortalEnrolmentController;
use App\User;
use App\Model\IntroCoursesModel as IntroCourses;
use App\Model\LmsCoursesModel as LMSCourses;

class StudentController extends Controller
{
    public function student() {
      // users
      // intro_courses
      // courses
      // indication if intro courses are done

      $student = Auth::user();

      $student->introCourses;
      $student->courses;
      $student->role = $student->roles->role;

      $introCoursesDonecounter = 0;
      foreach ($student->introCourses as $introCourse) {
        if ($introCourse->status == 2) {
          $introCoursesDonecounter++;
        }
      }
      if (count($student->introCourses) == $introCoursesDonecounter) {
        $student->introCoursesDone = true;
      }
      else {
        $student->introCoursesDone = false;
      }

      $payments = $student->payments;
      $student->identifications;
      $student->usi;


      // unset the irrelevant data
      unset($student->roles);
      unset($student->payments);

      return view('portal.student.student', compact('student', 'payments'));
    }
    public function qualifications() {
      return view('portal.student.previousQualification');
    }
}
