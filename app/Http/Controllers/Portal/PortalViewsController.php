<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortalViewsController extends Controller
{
  public function adminClients() {
    return view('portal.admin.clients.list');
  }
  public function adminClientsShow($id) {
    return view('portal.admin.clients.show', compact('id'));
  }
  public function developer() {
    return view('portal.admin.developer.developer');
  }
  public function enrolmentSettings() {
    return view('portal.enrolment.settings');
  }
}
