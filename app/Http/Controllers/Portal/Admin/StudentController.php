<?php

namespace App\Http\Controllers\Portal\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
  public function index() {
    return view('portal.admin.student.list');
  }
  public function show($id) {
    return view('portal.admin.student.show', compact('id'));
  }
}
