<?php

namespace App\Http\Controllers\Portal\Enrolment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Auth;
use App\Model\UserPaymentsModel;
use App\Model\StudentUSIModel;
use App\Model\EnrolmentModel;
use App\Model\EnrolmentStatusLogModel;
use App\Model\UserRoleModel;

class StudentController extends Controller
{
  /**
   * Store a newly created resource in user.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store($id, Request $request) {
    // will need the entry from EnrolmentModel using $id | DONE
    // will need to post details of the one who approved to EnrolmentStatusLogModel | DONE
    // will need to create user | create foldername | DONE
    // will need to create user_role | DONE
    // will need to create a new function for the payment file and will be stored on the foldername created above
    // if there's a USI it will store on the student_usi
    $this->validate($request, [
      'file' => 'required|file',
      'folder' => 'required|string',
      'amount_paid' => 'required|integer',
      'total_amount_agreed' => 'required|integer|gte:amount_paid',
      'payment_description' => 'nullable',
      'filename' => 'required|string',
    ]);

    $entry = EnrolmentModel::find($id);
    $entry->status = 1;
    $entry->save();

    $student = new User;
    $student->first_name = $entry->firstname;
    $student->last_name = $entry->lastname;
    $student->email = $entry->email;
    $student->username = $this->username($entry->firstname, $entry->lastname);
    $student->password = bcrypt($this->password($entry->firstname, $entry->lastname, $entry->dateofbirth));
    $student->folder = $request->folder;
    $student->save();

    $confirmedBy = new EnrolmentStatusLogModel;
    $confirmedBy->entryid = $id;
    $confirmedBy->userid = Auth::user()->id;
    $confirmedBy->student_id = $student->id;
    $confirmedBy->status = 1;
    $confirmedBy->save();

    $role = new UserRoleModel;
    $role->user_id = $student->id;
    $role->role = 3; // 3 = student role
    $role->save();

    $payment = new UserPaymentsModel;
    $payment->user_id = $student->id;
    $payment->course_id = 1; // change
    $payment->amount_paid = $request->amount_paid;
    $payment->total_amount_agreed = $request->total_amount_agreed;
    if ($request->payment_description) {
      $payment->description = $request->payment_description;
    }
    $payment->filename = $request->filename;
    $payment->save();

    if ($entry->usi) {
      $usi = new StudentUSIModel;
      $usi->user_id = $student->id;
      $usi->usi_number = $entry->usinumber;
      $usi->status = 0;
      $usi->save();
    }

    $path = $request->file('file')->storeAs('public/students/'.$request->folder, $request->filename);

    $getConfirmedBy = Auth::user();

    $confirmedBy->first_name = $getConfirmedBy->first_name;
    $confirmedBy->last_name = $getConfirmedBy->last_name;

    return $confirmedBy;
  }

  private function password($first, $last, $dob) {
    $firstChar = strtolower(substr($first, 0, 1));
    $secondChar = strtoupper(substr($last, 0, 1));
    $password = $firstChar.$secondChar.'.'.date('dmy', strtotime($dob));
    return $password;
  }

  private function username($first, $last) {
    $firstname = str_replace(' ', '', $first);
    $lastname = str_replace(' ', '', $last);
    $firstnamelower = strtolower($firstname);
    $lastnamelower = strtolower($lastname);
    $username = $firstnamelower.'.'.$lastnamelower;

    return $username;
  }
}
