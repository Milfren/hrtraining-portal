<?php

namespace App\Http\Controllers\Portal\Course;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CourseCategoryModel;

class CourseCategoryController extends Controller
{
  public function getCategory() {
    return CourseCategoryModel::all();
  }
  public function addCategory(Request $request) {
    $addCategory = new CourseCategoryModel;

    $addCategory->title = $request->title;
    $addCategory->short_name = $request->short_name;
    $addCategory->slug = str_slug($request->title);
    if ($request->description) {
      $addCategory->description = $request->description;
    }

    $addCategory->save();

    return $this->getCategory();
  }
  public function editCategory(Request $request) {
    $editCategory = CourseCategoryModel::find($request->id);

    $editCategory->title = $request->title;
    $editCategory->short_name = $request->short_name;
    $editCategory->slug = str_slug($request->title);
    $editCategory->description = $request->description;

    $editCategory->save();

    return $this->getCategory();
  }
  public function deleteCategory(Request $request) {
    $deleteCategory = CourseCategoryModel::find($request->id)->delete();

    return $this->getCategory();
  }
}
