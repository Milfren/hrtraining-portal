<?php

namespace App\Http\Controllers\Portal\Course;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CourseListModel;
use App\Model\UnitListModel;
use App\Model\UnitSectionModel;

class CourseListController extends Controller
{
    public function getCourses($categoryid) {
      return CourseListModel::where('categoryid', $categoryid)->get();
    }
    public function addCourses(Request $request) {
      $addCourse = new CourseListModel;

      $addCourse->categoryid = $request->categoryid;
      $addCourse->code = $request->code;
      $addCourse->title = $request->title;
      if ($request->description) {
        $addCourse->description = $request->description;
      }
      $addCourse->slug = str_slug($request->title).'-'.str_slug($request->code);
      $addCourse->visible = $request->visible;

      $addCourse->save();

      return $this->getCourses($request->categoryid);
    }
    public function editCourses(Request $request) {
      $editCourse = CourseListModel::find($request->id);
      $editCourse->code = $request->code;
      $editCourse->title = $request->title;
      $editCourse->description = $request->description;
      $editCourse->visible = $request->visible;
      $editCourse->slug = str_slug($request->title).'-'.str_slug($request->code);

      $editCourse->save();

      return $this->getCourses($request->categoryid);
    }
    public function deleteCourses(Request $request) {
      $deleteCourse = CourseListModel::find($request->id)->delete();
      $deleteUnits = UnitListModel::where('courseid', $request->id)->delete();
      $deleteSections = UnitSectionModel::where('courseid', $request->id)->delete();
      
      return $this->getCourses($request->categoryid);
    }
}
