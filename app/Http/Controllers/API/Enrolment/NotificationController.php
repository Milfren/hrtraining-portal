<?php

namespace App\Http\Controllers\API\Enrolment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\FormNotificationsModel as Notification;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $notif = new Notification;
      $notif->form_id = $request->form_id;
      $notif->type = $request->type;
      $notif->email = $request->email;
      $notif->save();

      return $this->show($request->form_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return Notification::where('form_id', $id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $notif = Notification::find($id);
      $form_id = $notif->form_id;
      $notif->delete();

      return $this->show($form_id);
    }
}
