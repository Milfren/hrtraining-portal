<?php

namespace App\Http\Controllers\API\Enrolment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\cURLController;

use Auth;
use App\Model\EnrolmentModel as Enrolment;
use App\Model\EnrolmentStatusLogModel as EnrolmentStatus;

// Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeEnrolment;
use App\Mail\RPLWelcomeLetter;

class EnrolmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return Enrolment::all('id', 'firstname', 'lastname', 'status');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request->fields;

      $enrol = new Enrolment;
      $enrol->status = 0;
      $enrol->firstname = $request->fields['firstname'];
      $enrol->lastname = $request->fields['lastname'];
      $enrol->dateofbirth = $request->fields['dateofbirth'];
      $enrol->sex = $request->fields['sex'];
      $enrol->usi = $request->fields['usi'];
      if ($request->fields['usi']) {
        $enrol->usinumber = $request->fields['usinumber'];
        $enrol->usiagree = true;
      }

      $enrol->street_address = $request->fields['residentAdd']['street_address'];
      $enrol->city = $request->fields['residentAdd']['city'];
      $enrol->state = $request->fields['residentAdd']['state'];
      $enrol->postal_code = $request->fields['residentAdd']['postal_code'];
      $enrol->country = $request->fields['residentAdd']['country'];
      $enrol->postal_same = $request->fields['postal_same'];

      if (!$request->fields['postal_same']) {
        $enrol->postal_street_address = $request->fields['postalAdd']['street_address'];
        $enrol->postal_city = $request->fields['postalAdd']['city'];
        $enrol->postal_state = $request->fields['postalAdd']['state'];
        $enrol->postal_postal_code = $request->fields['postalAdd']['postal_code'];
        $enrol->postal_country = $request->fields['postalAdd']['country'];
      }

      $enrol->home_number = $request->fields['home_number'];
      $enrol->mobile_number = $request->fields['mobile_number'];
      $enrol->email = $request->fields['email'];
      $enrol->course_category = $request->fields['course_category']['name'];
      $enrol->course_name = $request->fields['course_name'];
      $enrol->rpl = $request->fields['rpl'];
      $enrol->country_born = $request->fields['country_born'];
      if ($request->fields['language']['other'] != null && $request->fields['language']['other'] != '') {
        $enrol->language = $request->fields['language']['other'];
      }
      else {
        $enrol->language = 'English Only';
      }
      $enrol->english = $request->fields['english'];
      $enrol->aboriginal = $request->fields['aboriginal'];
      $enrol->disability = $request->fields['disability'];

      $disabilities = '';

      foreach ($request->fields['disabilities'] as $disability) {
        if ($disability['status']) {
          $disabilities = $disabilities . $disability['id'];
        }
      }
      $enrol->disabilities = $disabilities;
      $enrol->school_level = $request->fields['school_level'];
      $enrol->year_school_level = $request->fields['year_school_level'];
      $enrol->still_attending_school = $request->fields['still_attending_school'];
      $enrol->prior_qualification = $request->fields['prior_qualification'];

      $qualifications = '';
      foreach ($request->fields['qualifications'] as $qualification) {
        if ($qualification['status']) {
          $qualifications = $qualifications . $qualification['id'];
        }
      }
      $enrol->qualifications = $qualifications;
      $enrol->employment_status = $request->fields['employment_status'];
      $enrol->study_reason = $request->fields['study_reason'];
      $enrol->employer_title = $request->fields['employer_title'];
      $enrol->company_name = $request->fields['company_name'];
      $enrol->company_street = $request->fields['company_street'];
      $enrol->company_city = $request->fields['company_city'];
      $enrol->company_state = $request->fields['company_state'];
      $enrol->company_postal_code = $request->fields['company_postal_code'];
      $enrol->work_phone_number = $request->fields['work_phone_number'];
      $enrol->work_mobile_number = $request->fields['work_mobile_number'];
      $enrol->work_email = $request->fields['work_email'];

      $enrol->first_aid_cert = $request->fields['first_aid_cert'];
      $enrol->photo_emailed = $request->fields['photo_emailed'];

      $identifications = '';
      foreach ($request->fields['identification'] as $identification) {
        if ($identification['status']) {
          $identifications = $identifications . $identification['id'];
        }
      }
      $enrol->identification = $identifications;
      $enrol->cert_security = $request->fields['cert_security'];
      $enrol->security_license = $request->fields['security_license'];

      $documents = '';
      foreach ($request->fields['documents'] as $document) {
        if ($document['status']) {
          $documents = $documents . $document['id'];
        }
      }
      $enrol->documents = $documents;
      $enrol->fullname_declaration = $request->fields['fullname_declaration'];
      $enrol->certify_agreement = $request->fields['certify_agreement'];
      $enrol->dateofenrolment = $request->fields['dateofenrolment'];
      $enrol->discovered = $request->fields['discovered'];
      $enrol->ip_address = $request->ip();


      $enrol->save();
      return $enrol;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return Enrolment::where('id', $id)
      ->with(['statusLog.user:id,first_name,last_name'])->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $enrol = Enrolment::find($id);
      $enrol->status = 1;
      $enrol->save();

      $status = new EnrolmentStatus;
      $status->entryid = $id;
      $status->userid = Auth::user()->id;
      $status->status = 1;
      $status->save();

      //LMS interaction
      $this->initialStudentSetup($enrol, $request->credu, $request->credp, $request->fields);

      // send a welcome email
      $data = new \stdClass();
      $data->firstname = $enrol->firstname;
      $data->lastname = $enrol->lastname;
      $data->sex = $enrol->sex;
      $data->credu = $request->credu;
      $data->credp = $request->credp;

      $this->sendWelcomeLetter($enrol->email, $request->emails, [], $request->fields['welcomeLetter'], $data);

      return Enrolment::where('id', $id)
      ->with(['statusLog.user:id,first_name,last_name'])->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Enrolment::find($id)->delete();
      return Enrolment::all('id', 'firstname', 'lastname', 'status');
    }

    private function callCurl($functionName, $restformat,  $params, $url = null) {
      header('Content-Type: text/plain');
      $serverurl = env('LMS_DOMAIN') . '/webservice/rest/server.php'. '?wstoken=' . env('LMS_TOKEN') . '&wsfunction='.$functionName;

      $curl = new cURLController;
      //if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
      $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
      return $resp = $curl->post($serverurl . $restformat, $params);
    }

    private function core_user_get_users($key, $value) {
      $functionName = 'core_user_get_users';

      $getUser = new \stdClass();
      $getUser->key = $key;
      $getUser->value = $value;
      $getUsers = array($getUser);
      $params = array('criteria' => $getUsers);

      return $resp = $this->callCurl($functionName, 'json', $params);
    }

    private function core_user_create_users($entry, $username, $password) {
      $functionName = 'core_user_create_users';

      $user = new \stdClass();
      $user->username = $username;
      $user->password = $password;
      $user->firstname = $entry->firstname;
      $user->lastname = $entry->lastname;
      // change email
      $user->email = $entry->email;
      $users = array($user);
      $params = array('users' => $users);

      return $this->callCurl($functionName, 'json', $params);
    }

    private function enrol_manual_enrol_users($userid, $courseid) {
      $functionName = 'enrol_manual_enrol_users';

      $enrolUser = new \stdClass();
      $enrolUser->roleid = '5';
      $enrolUser->userid = $userid;
      $enrolUser->courseid = $courseid;
      $enrolUsers = array($enrolUser);
      $params = array('enrolments' => $enrolUsers);

      return $this->callCurl($functionName, 'json', $params);
    }

    private function initialStudentSetup($entry, $username, $password, $request) {
      // Check if username is already taken
      $firstCheck = json_decode($this->core_user_get_users('username', $username));
      // Check if email is already taken
      $secondCheck = json_decode($this->core_user_get_users('email', $entry->email));
      // if both returns false, proceed to create a user in the LMS
      if (!count($firstCheck->users) && !count($secondCheck->users)) {
        $enrolled = json_decode($this->core_user_create_users($entry, $username, $password));
        $enrolled = $enrolled[0];

        // 1 = Generic, 2 = EQ, 3 = RPL
        // Enrol in LLN if Generic / EQ
        if ($request['welcomeLetter'] == 1 || $request['welcomeLetter'] == 2) {
          $this->enrol_manual_enrol_users($enrolled->id, '877');
        }
        // Enrol every student in LLN
        $this->enrol_manual_enrol_users($enrolled->id, '689');
      }
    }

    private function sendWelcomeLetter($email, $cc, $bcc, $type, $data) {
      $bccStatic = ['nerflimjohn@gmail.com'];
      if ($type == 1) {
        Mail::to($email)
        ->cc($cc)
        ->bcc($bccStatic)
        ->send(new WelcomeEnrolment($data));
        return 'email sent';
      }
      elseif ($type == 2) {
        Mail::to($email)
        ->cc($cc)
        ->bcc($bccStatic)
        ->send(new WelcomeEnrolment($data));
        return 'email sent';
      }
      elseif ($type == 3) {
        Mail::to($email)
        ->cc($cc)
        ->bcc($bccStatic)
        ->send(new RPLWelcomeLetter($data));
        return 'email sent';
      }
    }
}
