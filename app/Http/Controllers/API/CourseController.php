<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CourseListModel;
use App\Model\UnitSectionModel;
use App\Model\UnitListModel;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->type == 'section') {
        $request->validate([
            'courseid' => 'required|integer',
            'section.title' => 'required|string',
            'section.description' => 'nullable'
        ]);

        $section = new UnitSectionModel;
        $section->courseid = $request->courseid;
        $section->title = $request->section['title'];
        if ($request->section['description']) {
          $section->description = $request->section['description'];
        }
        $section->save();
      }
      elseif ($request->type == 'unit') {
        $request->validate([
            'courseid' => 'required|integer',
            'section.sectionid' => 'required|integer',
            'section.code' => 'nullable|string',
            'section.title' => 'nullable|string',
            'section.note' => 'nullable||string',
            'section.visible' => 'required|boolean'
        ]);

        $unit = new UnitListModel;
        $unit->courseid = $request->courseid;
        $unit->sectionid = $request->section['sectionid'];
        if ($request->section['code']) {
          $unit->code = $request->section['code'];
        }
        if ($request->section['title']) {
          $unit->title = $request->section['title'];
        }
        if ($request->section['note']) {
          $unit->note = $request->section['note'];
        }
        $unit->visible = $request->section['visible'];
        $unit->save();
      }
      return $this->show($request->courseid);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $course = CourseListModel::find($id);
      $course->sections;
      foreach ($course->sections as $section) {
        $section->units = UnitListModel::where('sectionid', $section->id)->get();
      }

      return $course;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
      if ($request->type == 'section') {
        $request->validate([
            'section.id' => 'required|integer',
            'section.title' => 'required|string',
            'section.description' => 'nullable'
        ]);

        $section = UnitSectionModel::find($request->section['id']);
        $section->courseid = $id;
        $section->title = $request->section['title'];
        if ($request->section['description']) {
          $section->description = $request->section['description'];
        }
        $section->save();
      }
      elseif ($request->type == 'unit') {
        $request->validate([
            'section.courseid' => 'required|integer',
            'section.sectionid' => 'required|integer',
            'section.code' => 'nullable|string',
            'section.title' => 'nullable|string',
            'section.note' => 'nullable||string',
            'section.visible' => 'required|boolean'
        ]);

        $unit = UnitListModel::find($request->section['id']);
        $unit->courseid = $id;
        $unit->sectionid = $request->section['sectionid'];
        if ($request->section['code']) {
          $unit->code = $request->section['code'];
        }
        if ($request->section['title']) {
          $unit->title = $request->section['title'];
        }
        if ($request->section['note']) {
          $unit->note = $request->section['note'];
        }
        $unit->visible = $request->section['visible'];
        $unit->save();
      }
      return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      $request->validate([
          'id' => 'required|integer'
      ]);
      if ($request->type == 'section') {
        UnitSectionModel::find($request->id)->delete();
        UnitListModel::where('sectionid', $request->id)->delete();
      }
      elseif ($request->type == 'unit') {
        UnitListModel::find($request->id)->delete();
      }
      return $this->show($id);
    }
}
