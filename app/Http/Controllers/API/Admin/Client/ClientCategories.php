<?php

namespace App\Http\Controllers\API\Admin\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\ClientCategoriesModel as Categories;
use App\Model\ClientCategoriesStudentModel as Student;

class ClientCategories extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $category = new Categories;
      $category->name = $request->name;
      $category->client_id = $request->client_id;
      $category->save();

      return Categories::where('client_id', $request->client_id)->with('students.user')->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $category = Categories::find($id);
      $category->name = $request->name;
      $category->save();

      return Categories::where('client_id', $category->client_id)->with('students.user')->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $category = Categories::find($id);
      $client_id = $category->client_id;
      $category->delete();
      Student::where('category_id', $id)->delete();
      
      return Categories::where('client_id', $client_id)->with('students.user')->get();
    }
}
