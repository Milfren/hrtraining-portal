<?php

namespace App\Http\Controllers\API\Admin\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\ClientCategoriesStudentModel as Student;

class ClientCategoriesStudent extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      foreach ($request->students as $stud) {
        $student = new Student;
        $student->user_id = $stud['user_id'];
        $student->category_id = $request->category_id;
        $student->client_students_id = $stud['id'];
        $student->save();
      }

      return Student::where('category_id', $request->category_id)->with('user')->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $student = Student::find($id);
      $category_id = $student->category_id;
      $student->delete();

      return Student::where('category_id', $category_id)->with('user')->get();
    }
}
