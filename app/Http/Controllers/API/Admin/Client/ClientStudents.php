<?php

namespace App\Http\Controllers\API\Admin\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\ClientStudentsModel;
use App\User;
use App\Model\ClientCategoriesStudentModel as Student;
use App\Model\ClientModel as Client;

class ClientStudents extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $users = User::where('first_name', 'like', $request->name.'%')
        ->orWhere('last_name', 'like', $request->name.'%')
        ->limit(10)
        ->orderBy('first_name', 'asc')
        ->get();

      return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $student = new ClientStudentsModel;
      $student->user_id = $request->user_id;
      $student->client_id = $request->client_id;
      $student->save();

      return ClientStudentsModel::where('client_id', $request->client_id)->with('user')->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return 'show';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $student = ClientStudentsModel::find($id);
      $client_id = $student->client_id;
      $student->delete();

      Student::where('client_students_id', $id)->delete();

      return Client::where('id', $client_id)->with(['students.user', 'categories.students.user'])->first();
    }
}
