<?php

namespace App\Http\Controllers\API\Admin\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\ClientFilesModel as ClientFile;
use App\Model\ClientModel as Client;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ClientFiles extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $client = Client::find($request->client_id);

      $file = new ClientFile;
      $file->client_id = $request->client_id;
      $file->name = $request->filename;
      $file->filename = $request->filename;
      $file->type = $request->type;
      $file->uploaded_by = Auth::id();
      $file->updated_by = Auth::id();
      $file->save();

      $path = $request->file('file')->storeAs('public/clients/'.$client->folder, $request->filename);

      return ClientFile::where('client_id', $request->client_id)->with([
        'updated_by:id,first_name,last_name',
        'uploaded_by:id,first_name,last_name'
      ])->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $file = ClientFile::find($id);
      $client = Client::find($file->client_id);

      Storage::delete('public/clients/'.$client->folder.'/'.$file->filename);
      $file->delete();
      return ClientFile::where('client_id', $client->id)->with([
        'updated_by:id,first_name,last_name',
        'uploaded_by:id,first_name,last_name'
      ])->get();
    }
}
