<?php

namespace App\Http\Controllers\API\Admin\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\ClientModel as Client;
use App\Model\ClientStudentsModel;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return Client::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $client = new Client;
      $client->name = $request->name;
      if ($request->description) {
        $client->description = $request->description;
      }
      $client->save();
      $client->folder = 'client'.str_random(10).$client->id;
      $client->save();

      return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return Client::where('id', $id)->with([
        'students.user',
        'categories.students.user',
        'files.updated_by:id,first_name,last_name', 
        'files.uploaded_by:id,first_name,last_name'
      ])->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $client = Client::find($id);
      $client->name = $request->name;
      $client->description = $request->description;
      $client->save();

      return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Client::find($id)->delete();
      return $this->index();
    }
}
