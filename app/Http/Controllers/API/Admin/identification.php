<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\IdentificationsModel as Identifications;
use App\User;

class identification extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->id) {
        return $this->update($request, $request->id);
      }

      $id = new Identifications;
      $id->user_id = $request->user_id;
      $id->id_name = $request->id_name;
      $id->type = $request->type;
      if($request->note) {
        $id->note = $request->note;
      } else {
        $id->note = '';
      }
      if($request->hasFile('file') && $request->type == 1) {
        $id->filename = $request->filename;
        $id->status = 2;
        $user = User::find($request->user_id);
        $path = $request->file('file')->storeAs('public/students/'.$user->folder, $request->filename);
      }
      elseif($request->type == 0) {
        $id->status = 0;
      }
      $id->save();
      $user_ids = Identifications::where('user_id', $id->user_id)->get();

      return $user_ids;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * Verified identifications cannot be changed
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $id = Identifications::find($id);
      if ($request->method == 'edit') {
        $id->id_name = $request->id_name;
        $id->type = $request->type;
        if($request->note) {
          $id->note = $request->note;
        } else {
          $id->note = '';
        }
        if($request->hasFile('file') && $request->type == 1) {
          $id->filename = $request->filename;
          $id->status = 2;
          $user = User::find($id->user_id);
          $path = $request->file('file')->storeAs('public/students/'.$user->folder, $request->filename);
        }
        elseif($request->type == 0) {
          $id->status = 0;
        }
        $id->save();
      } else { // this will do the approval
        $id->status = 2;
        $id->save();
      }

      $user_ids = Identifications::where('user_id', $id->user_id)->get();

      return $user_ids;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $id = Identifications::find($id);
      $user_id = $id->user_id;
      //
      if($id->status != 2) {
        $id->delete();
      }
      $user = Identifications::where('user_id', $user_id)->get();
      return $user;
    }
}
