<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CourseCategoryModel;
use App\Model\CourseListModel;
use App\Model\UnitSectionModel;
use App\Model\UnitListModel;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return CourseCategoryModel::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'category.title' => 'required|string',
        'category.short_name' => 'required|string',
        'category.description' => 'nullable'
      ]);

      $category = new CourseCategoryModel;

      $category->title = $request->category['title'];
      $category->short_name = $request->category['short_name'];
      $category->slug = str_slug($request->category['title']);
      if ($request->category['description']) {
        $category->description = $request->category['description'];
      }
      $category->save();

      return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'category.title' => 'required|string',
        'category.short_name' => 'required|string',
        'category.description' => 'nullable'
      ]);

      $category = CourseCategoryModel::find($id);

      $category->title = $request->category['title'];
      $category->short_name = $request->category['short_name'];
      $category->slug = str_slug($request->category['title']);
      $category->description = $request->category['description'];
      $category->save();

      return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $courses = CourseListModel::where('categoryid', $id)->get();
      if ($courses) {
        foreach ($courses as $course) {
          $section = UnitSectionModel::where('courseid', $course->id)->delete();
          $units = UnitListModel::where('courseid', $course->id)->delete();
        }
      }
      $courses = CourseListModel::where('categoryid', $id)->delete();
      $category = CourseCategoryModel::find($id)->delete();

      return $this->index();
    }
}
