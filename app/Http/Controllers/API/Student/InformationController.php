<?php

namespace App\Http\Controllers\API\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\PortalEnrolmentController;
use App\User;
use App\Model\IntroCoursesModel as IntroCourses;
use App\Model\LmsCoursesModel as LMSCourses;

use App\Model\EnrolmentModel;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     * Always json_decode the API response from LMS
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // users
      // intro_courses
      // courses
      // indication if intro courses are done

      $student = User::find($id);

      $student->introCourses;
      $student->courses;
      $student->role = $student->roles->role;

      $introCoursesDonecounter = 0;
      foreach ($student->introCourses as $introCourse) {
        if ($introCourse->status == 2) {
          $introCoursesDonecounter++;
        }
      }
      if (count($student->introCourses) == $introCoursesDonecounter) {
        $student->introCoursesDone = true;
      }
      else {
        $student->introCoursesDone = false;
      }

      $student->payments;
      $student->identifications;
      $student->usi;


      // unset the irrelevant data
      unset($student->roles);

      return $student;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
