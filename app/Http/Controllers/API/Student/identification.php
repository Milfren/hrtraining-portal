<?php

namespace App\Http\Controllers\API\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\IdentificationsModel as Ids;
use App\User;

class identification extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      return $this->update($request, $request->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $ids = Ids::find($id);
      $ids->filename = $request->filename;
      $ids->status = 1;
      $ids->save();

      $user = User::find($ids->user_id);
      $path = $request->file('file')->storeAs('public/students/'.$user->folder, $request->filename);

      $response = Ids::where('user_id', $ids->user_id)->get();

      return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
