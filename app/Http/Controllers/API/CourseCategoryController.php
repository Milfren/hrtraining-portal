<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\CourseCategoryModel;
use App\Model\CourseListModel;
use App\Model\UnitSectionModel;
use App\Model\UnitListModel;

class CourseCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'categoryid' => 'required|integer',
        'course.code' => 'required|string',
        'course.title' => 'required|string',
        'course.description' => 'nullable',
        'course.visible' => 'required|boolean'
      ]);

      $course = new CourseListModel;
      $course->categoryid = $request->categoryid;
      $course->code = $request->course['code'];
      $course->title = $request->course['title'];
      $course->slug = str_slug($request->course['title']);
      if ($request->course['description']) {
        $course->description = $request->course['description'];
      }
      $course->visible = $request->course['visible'];

      $course->save();

      return $this->show($request->categoryid);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $courses = CourseCategoryModel::find($id);
      $courses->courses;

      return $courses;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'course.id' => 'required|integer',
        'course.code' => 'required|string',
        'course.title' => 'required|string',
        'course.description' => 'nullable',
        'course.visible' => 'required|boolean'
      ]);

      $course = CourseListModel::find($request->course['id']);
      $course->categoryid = $id;
      $course->code = $request->course['code'];
      $course->title = $request->course['title'];
      // $course->slug = $request->course['slug'];
      $course->description = $request->course['description'];
      $course->visible = $request->course['visible'];
      $course->save();

      return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      $request->validate([
          'courseid' => 'required|integer'
      ]);
      $course = CourseListModel::find($request->courseid)->delete();
      $sections = UnitSectionModel::where('courseid', $request->courseid)->delete();
      $units = UnitListModel::where('courseid', $request->courseid)->delete();
      return $this->show($id);
    }
}
