<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class FrontendController extends Controller
{
    public function home() {
      if (Auth::check()) {
        return redirect('/portal/enrolment');
      }
      return view('frontend.pages.home');
    }

    public function dashboard() {
      return view('frontend.pages.dashboard');
    }

}
