<?php

namespace App\Http\Controllers\Frontend;

use App\Model\EnrolmentModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\PortalEnrolmentController;

class EnrolmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'success';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $submitted = false;
        return view('frontend.pages.enrolmentForm', compact('submitted'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $submitted = true;
        $requests = $request;
        $enrol = new EnrolmentModel;
        $enrol->status = 0;
        $enrol->firstname = $request->firstName;
        $enrol->lastname = $request->lastName;
        $enrol->dateofbirth = $request->birthDate;
        $enrol->sex = $request->sexRadios;
        $enrol->usi = $request->usiRadios;
        if ($request->usiRadios) {
          $enrol->usinumber = $request->usiNumberInput;
        }
        $enrol->street_address = $request->residentialAddressInput1;
        $enrol->city = $request->residentialAddressInput2;
        $enrol->state = $request->residentialAddressInput3;
        $enrol->postal_code = $request->residentialAddressInput4;
        $enrol->country = $request->residentialAddressInput5;
        if ($request->postalAddress1) {
          $enrol->postal_same = $request->postalAddress1;
        }
        else {
          $enrol->postal_street_address = $request->postalAddressInput1;
          $enrol->postal_city = $request->postalAddressInput2;
          $enrol->postal_state = $request->postalAddressInput3;
          $enrol->postal_postal_code = $request->postalAddressInput4;
          $enrol->postal_country = $request->postalAddressInput5;
        }
        $enrol->home_number = $request->contactInfo1;
        $enrol->mobile_number = $request->contactInfo2;
        $enrol->email = $request->contactInfo3;
        $enrol->course_category = $request->courseCategorySelect;
        $enrol->course_name = $request->courseNameSelect;
        $enrol->rpl = $request->rplRadios;
        $enrol->country_born = $request->countryBornInput;
        $enrol->language = $request->englishOnly;
        $enrol->ip_address = $request->ip();

        $enrol->save();



        $id = 'hey';
        return view('frontend.pages.enrolmentForm', compact('submitted', 'requests', 'id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
