<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('portal.home');
    }


    public function test() {
      $tokenurl = 'https://hrtrainingcollege.com/webservice/rest/server.php?wstoken=bddfb9cfbf90cb85e6862e4eecf23b65&wsfunction=core_user_get_users&moodlewsrestformat=json&criteria[0][key]=id&criteria[0][value]=2';
      $tokenresponse = file_get_contents($tokenurl);
      $tokenobject = json_decode($tokenresponse);



      $users = DB::connection('mysql_lms')->table('mdl_user')->get();

      return view('portal.test', compact('tokenobject', 'users'));
      // return $tokenobject->users[0]->username;
    }

    public function completionReport() {
      $perfectgrades = DB::connection('mysql_lms')->table('mdl_grade_grades')->whereRaw('finalgrade = rawgrademax');
      // $perfectgrades = DB::select('select * from mdl_grade_grades where finalgrade = rawgrademax');


      // count how many assessments in a unit
      $mdl_grade_items = DB::connection('mysql_lms')->table('mdl_grade_items')
          ->join('mdl_course', 'mdl_grade_items.courseid', '=', 'mdl_course.id')
          ->select(DB::raw('mdl_course.id AS id, mdl_grade_items.courseid AS courseid, mdl_course.fullname AS fullname, count(*) AS count'))
          ->whereRaw('mdl_grade_items.hidden = 0 and mdl_grade_items.itemtype = "mod"')
          ->groupBy('courseid')
          ->get();

      // count how many perfect grades in a UNIT
      // insert connection('mysql_lms')-> after DB::
      $count_perfects = DB::connection('mysql_lms')->table('mdl_grade_grades')
        ->join('mdl_grade_items', 'mdl_grade_grades.itemid', '=', 'mdl_grade_items.id')
        ->join('mdl_user', 'mdl_grade_grades.userid', '=', 'mdl_user.id')
        ->join('mdl_course', 'mdl_grade_items.courseid', '=', 'mdl_course.id')
        ->select(DB::raw('mdl_grade_grades.id AS id, mdl_grade_grades.userid AS userid, CONCAT(mdl_user.firstname, " ", mdl_user.lastname) AS name, mdl_grade_grades.finalgrade AS grade, MAX(from_unixtime(mdl_grade_grades.timemodified)) AS time, mdl_grade_items.id AS itemid, CONCAT(mdl_grade_grades.userid, "-", mdl_course.id) AS usercourse, mdl_course.fullname AS coursename, mdl_course.id AS courseid, count(*) AS countsame'))
        ->whereRaw('mdl_grade_items.courseid != 877 and mdl_grade_grades.finalgrade = mdl_grade_grades.rawgrademax and mdl_grade_items.hidden = 0 and mdl_grade_grades.aggregationstatus != "unknown"')
        ->groupBy('usercourse')
        ->latest(DB::raw('from_unixtime(mdl_grade_grades.timemodified)'))
        ->get();

      $completed;
      $datacounter = 0;
      $datacounterz = 0;
      $datacounterzz = 0;

      // still need to fix the conditional statement
      // conditional statement not working properly
      foreach ($count_perfects as $count_perfect) {
        $datacounter++;
        foreach ($mdl_grade_items as $mdl_grade_item) {
          if($mdl_grade_item->count == $count_perfect->countsame && $mdl_grade_item->courseid == $count_perfect->courseid && $count_perfect->userid != null) {
            $completed[$datacounter] = $count_perfect;
          }
        }
      }
      $completedUsers = (object) $completed;


      return view('portal.completionReport', compact('mdl_grade_items', 'perfectgrades', 'count_perfects', 'completed', 'completedUsers'));
    }
    public function completionWithVue() {
        return view('portal.completionWithVue');
    }
    public function completionWithVueRequest() {
      $results = DB::connection('mysql_lms')->table('mdl_grade_grades')
        ->join('mdl_grade_items', 'mdl_grade_grades.itemid', '=', 'mdl_grade_items.id')
        ->join('mdl_user', 'mdl_grade_grades.userid', '=', 'mdl_user.id')
        ->join('mdl_course', 'mdl_grade_items.courseid', '=', 'mdl_course.id')
        ->whereRaw('mdl_grade_grades.finalgrade = mdl_grade_grades.rawgrademax')
        ->whereNull('mdl_grade_grades.rawgrade')
        ->where('mdl_grade_items.courseid', '!=', 877)
        ->select('mdl_grade_grades.*', 'mdl_user.firstname', 'mdl_user.lastname', 'mdl_course.fullname', 'mdl_grade_items.courseid')
        ->orderBy('mdl_grade_grades.timemodified', 'desc')
        ->paginate(200);
        return $results;
    }
}
