Vue.component('users', {
  template: '#report-template',

  props: ['list'],

  data: function () {
    return {
      searchName: '',
      sortBy: 'time',
      hiddenManual: false,
      removedAssessment: false
    };
  },

  methods: {
    changeOrder: function(sortKey) {
      this.sortBy = sortKey;
    },
    removeManual: function() {
      this.removedAssessment = !this.removedAssessment;
    }
  },

  filters: {
    filterManual: function(data) {
      var compare = _.includes(data, "Manual Assessment");

      if (compare === true) {

        return this.hiddenManual;
      }
      else {
        return data;
      }
      //

    }
  },

  created() {
    this.list = JSON.parse(this.list);
  }


});

new Vue({
  el: '#completionReport'
});
