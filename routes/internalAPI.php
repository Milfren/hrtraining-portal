<?php

// Passport not needed as we only need to be authenticated to use the API

use Illuminate\Http\Request;

Route::apiResources([
  'api/course-category' => 'API\CategoryController',
  'api/course-category/category' => 'API\CourseCategoryController',
  'api/course-category/category/course' => 'API\CourseController',
  'api/portal/my-information' => 'API\Student\InformationController',
  'api/student/info/id' => 'API\Student\identification',
]);

Route::group([
    'middleware' => 'role.admin',
    'prefix' => 'api',
], function() {
  Route::apiResources([
    '/admin/student' => 'API\Admin\StudentController',
    '/admin/student/usi' => 'API\Admin\usi',
    '/admin/student/info' => 'API\Admin\info',
    '/admin/student/id' => 'API\Admin\identification',
    '/admin/client' => 'API\Admin\Client\ClientController',
    '/admin/client-student' => 'API\Admin\Client\ClientStudents',
    '/admin/client-categories' => 'API\Admin\Client\ClientCategories',
    '/admin/client-categories-student' => 'API\Admin\Client\ClientCategoriesStudent',
    '/admin/client-files' => 'API\Admin\Client\ClientFiles',
    '/admin/enrolment-settings' => 'API\Enrolment\NotificationController',
  ]);
});

Route::group([
    'prefix' => 'api',
], function() {
  // Route::apiResources([
  //   '/admin/enrolment' => 'API\Enrolment\EnrolmentController'
  // ]);
  Route::resource('/admin/enrolment', 'API\Enrolment\EnrolmentController')->except([
    'store'
  ]);
});
