<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('portal.welcome');
// });

Auth::routes();

Route::group([
    'middleware' => 'auth',
    'prefix' => 'portal',
    'namespace' => 'Portal',
], function() {

  // Routes for Admin
  Route::group([
      'middleware' => 'role.admin',
      'prefix' => 'course-category',
  ], function() {
    Route::get('/', 'CourseController@category')->name('course.category');

    // COURSE PAGE DISPLAY
    Route::get('/{slug}', 'CourseController@list')->name('course.list');
    Route::get('/{slug}/{course}', 'CourseController@name')->name('course.name');

    Route::get('/list/course/edit', 'CourseController@edit')->name('course.edit');
    Route::get('/list/course/students', 'CourseController@students')->name('course.students');
    Route::get('/list/course/students/name', 'CourseController@studentsSub')->name('course.students.sub');
    Route::get('/list/course/course-offers', 'CourseController@offer')->name('course.offer');

    // Course Unit Page
    Route::get('/{slug}/{course}/units', 'CourseController@units')->name('course.units');
    Route::get('/list/course/website-settings', 'CourseController@website')->name('course.website');
  });

  Route::group([
      'middleware' => 'role.admin',
      'prefix' => 'students',
      'namespace' => 'Admin'
  ], function() {
    //Student
    Route::get('/', 'StudentController@index')->name('admin.students');
    Route::get('/{id}', 'StudentController@show')->name('admin.students.show');
  });

  Route::group([
      'middleware' => 'role.admin',
      'prefix' => 'clients',
  ], function() {

    Route::get('/', 'PortalViewsController@adminClients')->name('admin.clients.list');
    Route::get('/{id}', 'PortalViewsController@adminClientsShow')->name('admin.clients.show');
  });

  Route::group([
      'middleware' => 'role.admin',
      'prefix' => 'developer',
  ], function() {

    Route::get('/', 'PortalViewsController@developer')->name('admin.developer');
  });

  // Routes for Student
  Route::group([
      'middleware' => 'role.student',
  ], function() {
    //Student
    Route::get('/my-information', 'StudentController@student')->name('portal.student.information');
    Route::get('/my-previous-qualifications', 'StudentController@qualifications')->name('portal.student.qualifications');
  });
});

// Route::get('/home', 'Frontend\FrontendController@dashboard')->name('home');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'HomeController@test')->name('test');
Route::get('portal/completion-report-old', 'HomeController@completionReport')->name('completionReportOld');

Route::get('portal/completion-report-2-request', 'HomeController@completionWithVueRequest')->name('completionWithVueRequest');
Route::get('portal/emailtest', 'Portal\PortalEnrolmentController@emailTest');

// Portal Enrolment
Route::group([
    'middleware' => ['role.admin', 'auth'],
], function() {
  Route::get('portal/enrolment', 'Portal\PortalEnrolmentController@list')->name('enrolment.list');
  Route::get('portal/enrolment/{id}', 'Portal\PortalEnrolmentController@entry')->name('enrolment.entry');
  Route::get('portal/enrolment-settings', 'Portal\PortalViewsController@enrolmentSettings')->name('enrolment.settings');
  Route::get('portal/enrolTest', 'Portal\PortalEnrolmentController@enrolTest')->name('enrolment.test');
  Route::get('portal/completion-report', 'HomeController@completionWithVue')->name('completionReport');
});
// ADD A PAYMENT FILE API
Route::post('api/enrolment/payment', 'Portal\PortalEnrolmentController@addPayment')->name('enrolment.entry.confirm.payment');
Route::post('api/enrolment/payment/{entryid}', 'Portal\Enrolment\StudentController@store');



//Frontend
Route::get('/', 'Frontend\FrontendController@home')->name('frontend.home');

// Frontend Enrolment
Route::get('/enrolment/enrol', 'Frontend\EnrolmentController@create')->name('frontend.enrolmentForm');
Route::post('/enrolment/enrol', 'Frontend\EnrolmentController@store')->name('frontend.enrolment.post');
