@extends('portal.layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <completion-report-vue></completion-report-vue>
    </div>
  </div>
</div>
@endsection
