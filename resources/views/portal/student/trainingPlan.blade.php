<div class="content-block flush-padding">
  <div class="block-header">
    <i class="fas fa-book"></i> Training Plan Units of Study
  </div>
  <div class="block-body">
    <p class="text-success">Congratulations! You have completed your introductory courses, you may now proceed to your respective units below.</p>
    <div class="form-group">
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" checked>
        <label class="form-check-label" for="inlineCheckbox1">In Progress</label>
      </div>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" checked>
        <label class="form-check-label" for="inlineCheckbox2">Pending</label>
      </div>
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" checked>
        <label class="form-check-label" for="inlineCheckbox3">Completed</label>
      </div>
    </div>
    <p><a href="#">View Full Progress</a></p>
    <table class="table table-striped">
      <tbody>
        <tr>
          <td>Code</td>
          <td>Title</td>
          <td>Status</td>
          <td>Grade</td>
        </tr>
        <tr class="text-bold text-success">
          <td>CPPCMN3006</td>
          <td>Provide effective client service</td>
          <td>Completed</td>
          <td>100%</td>
        </tr>
        <tr>
          <td>CPPCMN3006</td>
          <td>Provide effective client service</td>
          <td>Pending</td>
          <td>--</td>
        </tr>
        <tr>
          <td>CPPCMN3006</td>
          <td>Provide effective client service</td>
          <td>In Progress</td>
          <td>30%</td>
        </tr>
        <tr class="text-bold text-success">
          <td>CPPCMN3006</td>
          <td>Provide effective client service</td>
          <td>Completed</td>
          <td>100%</td>
        </tr>
        <tr class="text-bold text-success">
          <td>CPPCMN3006</td>
          <td>Provide effective client service</td>
          <td>Completed</td>
          <td>100%</td>
        </tr>
        <tr class="text-bold text-success">
          <td>CPPCMN3006</td>
          <td>Provide effective client service</td>
          <td>Completed</td>
          <td>100%</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
