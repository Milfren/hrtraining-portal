@extends('portal.layouts.app')

@section('breadcrumb')
  {{-- <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Library</a></li>
      <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
  </nav> --}}
@endsection

@section('content')
<div class="content-block flush-padding previousQualification">
  <div class="block-header">
    <i class="fas fa-book"></i> My Previous Qualifications from HRtrainingcollege
  </div>
  <div class="block-body">
    <table class="table table-striped">
      <tbody>
        <tr>
          <td><strong>Code</strong></td>
          <td><strong>Course</strong></td>
          <td><strong>Completed on</strong></td>
          <td><strong>Option</strong></td>
        </tr>
        <tr>
          <td>CPP30316</td>
          <td>Certificate III in Cleaning Operations</td>
          <td>September 9, 2017</td>
          <td><a href="#">Request Soft Copy</a></td>
        </tr>
        <tr>
          <td>CPP20617</td>
          <td>Certificate II in Cleaning</td>
          <td>January 25, 2016</td>
          <td><a href="#">Request Soft Copy</a></td>
        </tr>
      </tbody>
    </table>
    <button type="button" class="btn btn-primary">Enrol a new Qualification</button>
  </div>
</div>
@endsection
