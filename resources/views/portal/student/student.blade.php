@extends('portal.layouts.app')

@section('breadcrumb')
  {{-- <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Library</a></li>
      <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
  </nav> --}}
@endsection

@section('content')
<div id="course-category" class="container">
    <div class="row">
      <div class="col-sm-12">
        <student-information :id="{{Auth::user()->id}}"></student-information>
        {{-- <div class="content-block bg-success text-white">
          Congratulations Mr. Milfren John dela Vega for completing Certificate III in Cleaning Operations. Your certificate is now on its way.
        </div>
        <p class="text-center">
          <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Show detailed tracking <i class="fas fa-caret-down"></i>
          </a>
        </p>
        <div class="collapse" id="collapseExample">
          <div class="content-block">
            <table class="table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th>Details</th>
                  <th>Date & time</th>
                  <th>Location</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Delivered</td>
                  <td>Tue 21 Aug • 08:49am</td>
                  <td>SAMFORD QLD</td>
                </tr>
                <tr>
                  <td>Awaiting Collection at SAMFORD LPO </td>
                  <td>Tue 21 Aug • 07:53am </td>
                  <td>SAMFORD QLD</td>
                </tr>
                <tr>
                  <td>In transit</td>
                  <td>Tue 21 Aug • 05:57am</td>
                  <td>FERNY HILLS QLD</td>
                </tr>
                <tr>
                  <td>Processed through Australia Post facility </td>
                  <td>Mon 20 Aug • 10:23pm </td>
                  <td>BRISBANE QLD</td>
                </tr>
                <tr>
                  <td>Received by Australia Post</td>
                  <td>Mon 20 Aug • 02:37pm</td>
                  <td>FORTITUDE VALLEY QLD</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div> --}}
      </div>
    </div>
</div>
@endsection
