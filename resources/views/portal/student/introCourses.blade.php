<div class="content-block flush-padding introCourses">
  <div class="block-header">
    <i class="fas fa-book"></i> Introductory Courses
  </div>
  <div class="block-body">
    <p class="text-danger">You need to finish the LLN and Induction Course. Note that you need to get 100% on the Induction course in order to proceed to your respective units of study</p>
    <table class="table table-bordered">
      <tbody>
        <tr>
          <td><strong>Course</strong></td>
          <td><strong>Status</strong></td>
          <td><strong>Percentage</strong></td>
        </tr>
        <tr class="table-warning">
          <td>Induction Course</td>
          <td>In Progress</td>
          <td>75%</td>
        </tr>
        <tr class="table-success">
          <td>LLN Review</td>
          <td>Completed</td>
          <td>80%</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
