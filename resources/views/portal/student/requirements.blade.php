<div class="content-block flush-padding requirements-container">
  <div class="block-header bg-danger">
    <i class="fas fa-file-alt"></i> REQUIREMENTS
  </div>
  <div class="block-body">
    <table class="table">
      <tbody>
        <tr>
          <td><strong>Document</strong></td>
          <td><strong>Status</strong></td>
        </tr>
        <tr>
          <td>Payment</td>
          <td class="warning">Partial/Paid</td>
        </tr>
        <tr>
          <td>Identification</td>
          <td class="success">Submitted</td>
        </tr>
        <tr>
          <td>USI</td>
          <td class="warning">Verification Issue</td>
        </tr>
        <tr>
          <td>Induction Course</td>
          <td class="success">100%</td>
        </tr>
        <tr>
          <td>LLN Review</td>
          <td class="success">75%</td>
        </tr>
        <tr>
          <td>Learner Survey</td>
          <td class="warning">Pending</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
