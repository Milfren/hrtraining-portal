@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      {{-- <li class="breadcrumb-item"><a href="{{ route('course.course') }}">Enrolment</a></li>
      <li class="breadcrumb-item"><a href="{{ route('course.edit') }}">CERTIFICATE II IN CLEANING CPP20617</a></li>
      <li class="breadcrumb-item"><a href="{{ route('course.students') }}">Students</a></li> --}}
      <li class="breadcrumb-item active" aria-current="page">Enrolment</li>
    </ol>
  </nav>
@endsection

@section('content')
  <div id="enrolment" class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <h2 class="text-uppercase title">Enrolments</h2>
        <div class="enrolment-container content-container">
          <enrolment-list></enrolment-list>
        </div>
      </div>
    </div>
  </div>

@endsection
