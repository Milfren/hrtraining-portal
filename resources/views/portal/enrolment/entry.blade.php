@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('enrolment.list') }}">Enrolment</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{$entry->firstname}} {{$entry->lastname}}</li>
    </ol>
  </nav>
@endsection

@section('content')
  {{-- Still need to display the exact values for view
  Needs to generate a PDF file
  Needs to generate an email confirmation --}}
  <div id="enrolment-entry" class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <enrolment-entry
          :entry-id="{{$entryId}}"
          password="{{ $password }}"
          @if ($entry->status)
            :confirmed-by="{{$confirmedBy}}"
          @endif
        ></enrolment-entry>
      </div>
    </div>
  </div>
@endsection
