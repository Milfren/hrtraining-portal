@extends('portal.layouts.app')

@section('content')
<div id="completionReport" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>

                <users list="{{ json_encode($completedUsers) }}"></users>

            </div>
        </div>
    </div>

    <template id="report-template">
      <div class="container">
        <input type="text" v-model="searchName" class="form-control mb-3" placeholder="Search students">
        <button type="button" class="btn btn-success" @click="removeManual">Remove Manual Assessment @{{removedAssessment}}</button>
        <table class="table table-bordered">
          <thead>
            <tr class="bg-info text-white">
              <th scope="col">COUNT</th>
              <th scope="col">USERID</th>
              <th scope="col">NAME</th>
              <th scope="col">USERCOURSE</th>
              <th scope="col">TIME</th>
              <th scope="col">COURSE</th>
            </tr>
          </thead>
          <tbody>
              <tr :class="{ 'removedManual': hiddenManual }" v-for="user in list | orderBy sortBy -1 | filterBy searchName in 'name'">
                <td>@{{ user.countsame }}</td>
                <td>@{{ user.userid }}</td>
                <td>@{{ user.name }}</td>
                <td><a href="https://hrtrainingcollege.com/grade/report/overview/index.php?userid=@{{ user.userid }}&id=1" target="_blank">@{{ user.usercourse }}</a></td>
                <td>@{{ user.time }}</td>
                {{-- <td>@{{ user.coursename | filterManual}} @{{hiddenManual}}</td> --}}
                <td>@{{ user.coursename}}</td>
              </tr>
          </tbody>
        </table>
      </div>
    </template>
</div>
@endsection
