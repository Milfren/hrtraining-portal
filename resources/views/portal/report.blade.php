@extends('portal.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                      <div class="col-sm-6">
                        <table class="table table-bordered">
                          <thead>
                            <tr class="bg-success">
                              <th scope="col">GRADE ID</th>
                              <th scope="col">COUNTER</th>
                              <th scope="col">NAME</th>
                              <th scope="col">USERCOURSE</th>
                              <th scope="col">TIME</th>
                              <th scope="col">ITEMID</th>
                              <th scope="col">COURSE</th>
                            </tr>
                          </thead>
                          <tbody>
                            {{ $datacounter }}
                            <br>
                            {{ $datacounterz }}
                            <br>
                            {{ $datacounterzz }}
                            <br>
                            {{ $count_perfects->count() }}
                            <br>
                            {{ $mdl_grade_items->count() }}


                            {{-- {{ $testdata[1]->fullname }} --}}
                            @foreach ($count_perfects as $count_perfect)
                              <tr>
                                <td>{{ $count_perfect->countsame }}</td>
                                <td>{{ $count_perfect->userid }}</td>
                                <td>{{ $count_perfect->name }}</td>
                                <td>{{ $count_perfect->usercourse }}</td>
                                <td>{{ $count_perfect->time }}</td>
                                <td>{{ $count_perfect->itemid }}</td>
                                <td>{{ $count_perfect->coursename }}</td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                      <div class="col-sm-6">
                        <table class="table table-bordered">
                          <thead>
                            <tr class="bg-success">
                              <th scope="col">COUNT</th>
                              <th scope="col">USERID</th>
                              <th scope="col">NAME</th>
                              <th scope="col">USERCOURSE</th>
                              <th scope="col">TIME</th>
                              <th scope="col">COURSE</th>
                            </tr>
                          </thead>
                          <tbody>
                            {{-- {{ $testdata[1]->fullname }} --}}
                            @foreach ($displayobjects as $displayobject)
                              <tr>
                                <td>{{ $displayobject->countsame }}</td>
                                <td>{{ $displayobject->userid }}</td>
                                <td>{{ $displayobject->name }}</td>
                                <td>{{ $displayobject->usercourse }}</td>
                                <td>{{ $displayobject->time }}</td>
                                <td>{{ $displayobject->coursename }}</td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
