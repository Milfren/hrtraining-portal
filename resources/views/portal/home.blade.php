@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Library</a></li>
      <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-category" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
          <h2 class="text-uppercase title">Cleaning Operations and Cleaning Management</h2>
          @for ($i=0; $i < 5; $i++)
            <div class="card category-item">
              <div class="card-body">
                <h5 class="card-title"><a href="#">Certificate II in Cleaning</a></h5>
                <p class="card-text"><strong>5 Courses</strong></p>
                <p class="card-text"><strong>20 STUDENTS | 10 IN PROGRESS | 10 COMPLETED</strong></p>
                <a href="#" class="card-link"><i class="fas fa-edit"></i> EDIT</a>
                <a href="#" class="card-link"><i class="far fa-eye-slash"></i> HIDE</a>
              </div>
            </div>
          @endfor
        </div>
    </div>
</div>
@endsection
