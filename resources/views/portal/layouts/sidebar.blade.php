<div id="sidebar-left">
  <div class="sidebar-brand">
    <img src="/assets/images/hrtraining_logo.png" class="img-fluid">
  </div>
  <div class="sidebar-content">
    <div class="list-group">
      @if (Auth::user()->roles->role == 3 || Auth::user()->roles->role == 5)
        {{-- student role --}}

        <a href="{{ route('portal.student.information') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('portal.student.information') ? 'active' : '' }}"><i class="fas fa-user"></i> My Information</a>
        <a href="{{ route('portal.student.qualifications') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('portal.student.qualifications') ? 'active' : '' }}"><i class="fas fa-book"></i> My Qualifications</a>

      @else
        <a href="#" class="hidden list-group-item list-group-item-action"><i class="fas fa-home"></i> Dashboard</a>
        <a href="{{ route('course.category') }}" class="hidden list-group-item list-group-item-action {{ Route::currentRouteNamed('course.category', 'course.edit', 'course.students', 'course.offer', 'course.units', 'course.website', 'course.students.sub') ? 'active' : '' }}"><i class="fas fa-sitemap"></i> Courses</a>
        <a href="{{route('admin.students')}}" class="hidden list-group-item list-group-item-action"><i class="fas fa-users"></i> Students</a>
        <a href="{{route('admin.clients.list')}}" class="hidden list-group-item list-group-item-action"><i class="fas fa-users"></i> Clients</a>
        <a href="{{ route('enrolment.list') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('enrolment.list', 'enrolment.entry') ? 'active' : '' }}"><i class="fas fa-users"></i> Enrolments</a>
        <a href="{{ route('completionReport') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('completionReport') ? 'active' : '' }}"><i class="fas fa-clipboard-list"></i> Reports</a>
        <a href="#" class="hidden list-group-item list-group-item-action"><i class="fas fa-layer-group"></i> Forms</a>
        <a href="#" class="hidden list-group-item list-group-item-action"><i class="fas fa-rss"></i> Blogs</a>
        <a href="#" class="hidden list-group-item list-group-item-action"><i class="fas fa-cog"></i> Settings</a>
        <a href="{{route('admin.developer')}}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('admin.developer') ? 'active' : '' }}"><i class="fas fa-cog"></i> Developer</a>
      @endif
    </div>
  </div>
</div>
