@extends('portal.layouts.app')

@section('breadcrumb')
  {{-- <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Library</a></li>
      <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
  </nav> --}}
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <admin-student-show id="{{$id}}"></admin-student-show>
    </div>
  </div>
</div>
@endsection
