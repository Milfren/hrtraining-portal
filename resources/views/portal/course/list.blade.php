@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Courses</a></li>
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Course Category</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{$getCategory->title}}</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-category" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <course-list
              route="{{ route('course.units', [$getCategory->slug, 'slug']) }}"
              :course-category="{{$getCategory}}">
            </course-list>
        </div>
    </div>
</div>
@endsection
