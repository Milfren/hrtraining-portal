@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Library</a></li>
      <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-course" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
          <h2 class="text-uppercase title">Certificate II in Cleaning CPP20617</h2>
                    <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="#">Edit Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.students') }}">Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Course Offers</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Units</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Website Settings</a>
            </li>
          </ul>

        </div>
    </div>
</div>
@endsection
