@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Courses</a></li>
      <li class="breadcrumb-item active" aria-current="page">Course Category</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-category" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
          <course-category
            course-list-route="{{ route('course.list', 'slug') }}">
          </course-category>
        </div>
    </div>
</div>
@endsection
