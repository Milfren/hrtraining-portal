@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Courses</a></li>
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Course Category</a></li>
      <li class="breadcrumb-item active" aria-current="page">Course List</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-course" class="container">
    <div class="row justify-content-center">
      <div class="col-sm-12">
        <h2 class="text-uppercase title">Certificate II in Cleaning CPP20617</h2>
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('course.edit') }}">Edit Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="{{ route('course.students') }}">Students</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('course.offer') }}">Course Offers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('course.units') }}">Units</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('course.website') }}">Website Settings</a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="row">
            <div class="col-sm-2">
              <img src="/assets/images/thumbnail.jpg" width="100%" alt="..." class="img-thumbnail">
            </div>
            <div class="col-sm-10">
              <h3 class="text-uppercase">Milfren John dela Vega</h3>
              <p>STATUS: In Progress | 50% | <a href="#" data-toggle="modal" data-target="#progressReportModal">View Progress Report</a></p>
              <p>COURSE OFFER: <a href="#">Course Offer Code Here</a></p>
            </div>
          </div>
          <table class="table table-bordered table-hover mt-3">
            <thead>
              <tr>
                <th scope="col">Code</th>
                <th scope="col">Title</th>
                <th scope="col">Grade</th>
                <th scope="col">Options</th>
              </tr>
            </thead>
            <tbody>
              @for ($i=0; $i < 5; $i++)
                <tr>
                  <th>CPPCCL2008</th>
                  <td>Clean carpeted floors</td>
                  <td class="text-center">
                    <span class="badge badge-success text-white">100%</span>
                    <br>25/07/2018
                  </td>
                  <td><a href="#" >View Progress</a></td>
                </tr>
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

@include('course.progress')

@endsection
