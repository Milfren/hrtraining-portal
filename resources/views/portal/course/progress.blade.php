<!-- Modal -->
<div class="modal fade" id="progressReportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">CPPCCL2008 Clean carpeted floors Progress Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-progress-report">
          <thead class="thead-dark">
            <tr>
              <th>Unit(s)/Module</th>
              <th>Assessment Types</th>
              <th>Grade</th>
              <th>Total Grade</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td rowspan="2" class="bg-light unit">(CPPSEC2011B) Control Access to and Exit from Premises</td>
              <td class="assessment-type">Multiple Choice</td>
              <td class="grade bg-success text-white">100.00 %</td>
              <td rowspan="2" class="total-grade text-center"><strong>50.00 %</strong></td>
            </tr>
            <tr>
              <td class="assessment-type">Written Questions</td>
              <td class="bg-danger text-white grade">0.00 % - needs grading</td>
            </tr>
            <tr>
              <td rowspan="2" class="bg-secondary text-white unit">(CPPSEC2011B) Control Access to and Exit from Premises</td>
              <td class="assessment-type">Multiple Choice</td>
              <td class="grade bg-success text-white">100.00 %</td>
              <td rowspan="2" class="total-grade text-center"><strong>50.00 %</strong></td>
            </tr>
            <tr>
              <td class="assessment-type">Written Questions</td>
              <td class="bg-danger text-white grade"><strong>0.00 % - no submission</strong></td>
            </tr>
            <tr>
              <td rowspan="2" class="bg-light unit">(CPPSEC2011B) Control Access to and Exit from Premises</td>
              <td class="assessment-type">Multiple Choice</td>
              <td class="grade bg-success text-white">100.00 %</td>
              <td rowspan="2" class="total-grade text-center"><strong>100.00 %</strong></td>
            </tr>
            <tr>
              <td class="assessment-type">Written Questions</td>
              <td class="bg-success text-white grade">100.00 %</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
