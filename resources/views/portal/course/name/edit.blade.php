@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Courses</a></li>
      <li class="breadcrumb-item active" aria-current="page">Course Category</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-course" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
          <h2 class="text-uppercase title">Certificate II in Cleaning CPP20617</h2>
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="{{ route('course.edit') }}">Edit Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.students') }}">Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.offer') }}">Course Offers</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.units') }}">Units</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.website') }}">Website Settings</a>
            </li>
          </ul>
          <div class="tab-content">
            <form>
              <p class="text-danger">The data here will reflect on the website</p>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-7">
                    <label for="courseName">Course Name</label>
                    <input type="text" class="form-control" id="courseName" aria-describedby="emailHelp" placeholder="Course Name">
                  </div>
                  <div class="col-sm-3">
                    <label for="courseCode">Course Code</label>
                    <input type="text" class="form-control" id="courseCode" placeholder="Course Code">
                  </div>
                  <div class="col-sm-2">
                    <label for="courseVisibile">Visibility</label>
                    <select class="form-control" id="courseVisibile">
                      <option>Visibile</option>
                      <option>Hidden</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="courseDescription">Description</label>
                <textarea class="form-control" id="courseDescription" rows="3"></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection
