@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Courses</a></li>
      <li class="breadcrumb-item"><a href="{{route('course.category')}}">Course Category</a></li>
      <li class="breadcrumb-item active" aria-current="page">Course List</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-course" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
          <h2 class="text-uppercase title">Certificate II in Cleaning CPP20617</h2>
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.edit') }}">Edit Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="{{ route('course.students') }}">Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.offer') }}">Course Offers</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.units') }}">Units</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.website') }}">Website Settings</a>
            </li>
          </ul>
          <div class="tab-content">
            <form>
              <div class="row">
                <div class="col-sm-9">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search a student enroled on this course" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <button type="button" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                </div>
              </div>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Firstname / Lastname</th>
                    <th scope="col">Status</th>
                    <th scope="col">Email</th>
                    <th scope="col">Last Access</th>
                  </tr>
                </thead>
                <tbody>
                  @for ($i=0; $i < 10; $i++)
                    <tr>
                      <td><a href="{{ route('course.students.sub') }}">Milfren John dela Vega</a></td>
                      <td><strong><span class="badge badge-success text-white">COMPLETED</span></strong></td>
                      <td>milfren@hrtraining.com.au</td>
                      <td>5 minutes</td>
                    </tr>
                  @endfor
                </tbody>
              </table>

              <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection
