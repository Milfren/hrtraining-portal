@extends('portal.layouts.app')

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('course.course') }}">Courses</a></li>
      <li class="breadcrumb-item"><a href="{{ route('course.edit') }}">CERTIFICATE II IN CLEANING CPP20617</a></li>
      <li class="breadcrumb-item active" aria-current="page">Course Offers</li>
    </ol>
  </nav>
@endsection

@section('content')
<div id="course-course" class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
          <h2 class="text-uppercase title">Certificate II in Cleaning CPP20617</h2>
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.edit') }}">Edit Settings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.students') }}">Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="{{ route('course.offer') }}">Course Offers</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.units') }}">Units</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('course.website') }}">Website Settings</a>
            </li>
          </ul>
          <div class="tab-content">
            course offers
          </div>
        </div>
    </div>
</div>
@endsection
