@component('mail::message')
  Dear <span class="text-capitalize"><strong>
    @if ($user->sex == 1)
      Mr
    @else
      Ms
    @endif {{$user->lastname}},
  </strong></span>

  Welcome to <em><strong>Human Resource Training</strong></em> and our Online Campus: <a href="https://hrtrainingcollege.com/" target="_blank">HRtrainingCollege</a>.

  To begin training or complete an RPL, all participants must complete a Language, Literacy & Numeracy (LLN) review as part of our code of practice as a responsible training provider.  This review has 8 tasks and most people are not expected to finish all 8 tasks in the allocated time of 18 mins.  For example, to reach the LLN standard for a certificate III you would only need to complete say 5 of the tasks.

  This LLN Review simply determines each participant’s ability to complete the level of certificate they have enrolled in – Certificate I to Certificate IV or the Diploma.

  Where participants do not reach the level of LLN for their certificate level, this means the Trainer is informed that you will need additional support to complete the certificate; it does not mean you will not be able to do the training.

  <strong>How to Start:</strong>

  Use this link to open our Online campus website: <a href="http://hrtrainingcollege.com" target="_blank">HRtrainingCollege.com</a>

  LOG-IN is in the right hand column (top)

  <strong>Your Username is:</strong>   <span class="text-red text-bold" style="margin-left: 10px;">{{$user->credu}}</span> (all lower case)
  <br><strong>Your password is:</strong>   <span class="text-red text-bold" style="margin-left: 10px;">{{$user->credp}}</span> (case sensitive) <span class="password-span text-red">Lowercase letter “<strong>{{$user->credp[0]}}</strong>” and capital “<strong>{{$user->credp[1]}}</strong>”</span>
  <br><span class="password-span">Note: You cannot change your User Name OR Password. This is done so we can access your username in the event we need to fix any IT problems you encounter.</span>

  When you are logged in, you’ll find a <strong>Navigation Bar</strong> in the right-hand column. Click on <strong>“Dashboard”</strong> and you will see the <strong> “LLN Review”</strong>.

  We entrust you enjoy your learning at our college and we look forward to helping you achieve your goals…

  Thanks for reading my message.

  <div>
    Regards,<br>
    <span class="footer-name">Jhun Syriell Acain</span>
    <br><strong>Administration Officer</strong>, Human Resource Training (RTO 31678)
    <br><span class="footer-span"><strong>Phone:</strong> +61 7 3289 4444</span> <span class="footer-span"><strong>Mobile:</strong> 0437 146 670</span> <span class="footer-span"><strong>Email:</strong> <a href="mailto:jhun@hrtraining.com.au">jhun@hrtraining.com.au</a></span>
    <br><span class="footer-span"><strong>Address:</strong> PO Box 503, Samford, Qld, 4520</span> <span class="footer-span"><strong>Website:</strong> <a href="https://hrtraining.com.au/" target="_blank">www.HRtraining.com.au</a></span>
  </div>
  <div>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/1.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/2.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/3.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/4.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/5.png') }}"></span>
  </div>
  <div class="footer-notice">
    This e-mail and any attachments are for authorised use by the intended recipient only. It may contain proprietary material, confidential information and/ or be subject to legal privilege. It, therefore, should not be copied,
    <br>disclosed to,  retained or used by any other party. If you are not an intended recipient please inform the sender by reply e-mail and promptly delete this e-mail and any attachments.
  </div>
  <div class="footer-company">
    HRD Consulting Australasia Pty Ltd trading as Human Resource Training  ACN 121 890 917  ABN 68 121 890 917
  </div>
@endcomponent
