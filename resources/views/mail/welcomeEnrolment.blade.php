@component('mail::message')
  Dear <span class="text-capitalize"><strong>
    @if ($user->sex == 1)
      Mr
    @else
      Ms
    @endif {{$user->lastname}},
  </strong></span>

  Welcome to <em><strong>Human Resource Training</strong></em> and our Online Campus: <a href="https://hrtrainingcollege.com/" target="_blank">HRtrainingCollege</a>.

  All participants need to complete the “<strong>Induction to HR training</strong>” course and an “<strong>LLN Review</strong>”. The Induction to HR training course acquaints you with our Leaning Management system (LMS), gives you information about your training, shows you how to complete assessments online and how to see your grades. There are 3 sample Assessment Tasks and these should take you less than 20mins to complete.  As with ALL assessment tasks in vocational training, <strong>you need to get 100% in each task</strong>, before moving onto the next task.  But you can make as many attempts as you need, with help from your HRtraining Mentor!  It doesn’t matter what your grade is when you submit your assessment for the first time…your Assessor will give you feedback after each attempt to help you gain 100%.  Just check your grades regularly and re-attempt any tasks that are not 100%.

  <strong>How to Start:</strong>

  Use this link to open our Online campus website: <a href="http://hrtrainingcollege.com" target="_blank">HRtrainingCollege.com</a>

  LOG-IN is in the right hand column (top)

  <strong>Your Username is:</strong>   <span class="text-red text-bold" style="margin-left: 10px;">{{$user->credu}}</span> (all lower case)
  <br><strong>Your password is:</strong>   <span class="text-red text-bold" style="margin-left: 10px;">{{$user->credp}}</span> (case sensitive) <span class="password-span text-red">Lowercase letter “<strong>{{$user->credp[0]}}</strong>” and capital “<strong>{{$user->credp[1]}}</strong>”</span>
  <br><span class="password-span">Note: You cannot change your User Name OR Password. This is done so we can access your username in the event we need to fix any IT problems you encounter.</span>

  When you are logged in, you’ll find a <strong>Navigation Bar</strong> in the right-hand column. Click on <strong>“Dashboard”</strong> and you will see the <strong> “Induction Course”</strong> listed with the <strong>“LLN Review”</strong>. Complete both.

  Begin the Induction course by reading all sections from top to bottom, complete the three (3) Assessment Tasks and view your Grades.

  Your first unit of study will be uploaded to the Dashboard after you complete the <strong>“Induction to HR training”</strong> course and <strong>“LLN Review”</strong> and as your course progresses new units will be uploaded each time you complete a unit of study.

  We suggest you begin by reading the Learner Guide, watching any slide presentation and looking at handouts or other information provided. Then you can commence the assessments by opening each assessment task from the <strong>Assessment Tasks</strong> section in the right hand column, OR you can scroll down the page and you will find the assessment tasks listed.

  Assessments may include:
  <ul>
    <li>Multiple Choice questions
      <br><span style="font-size: 11px;"><em>(answer these when you first open the unit, as they will only take you about 15 mins and are a good introduction)</em></span>
    </li>
    <li>Written Questions</li>
    <li>Projects or Scenarios</li>
    <li>Observation  Assessments</li>
    <li>Third Party Workplace Report completed by workplace Supervisor</li>
    <li>Videos to be created showing your practical skills.</li>
  </ul>

  Any issues you have with your training or challenges with the website can be sent to <a href="mailto:mentor@HRtraining.com.au">mentor@HRtraining.com.au</a>  See the link provided in the right hand column.

  To avoid losing any of your assessment responses, please regularly save as you complete each assessment task. The system will logout if you have not saved a response after 60 minutes.

  Thanks for reading my message.

  <div>
    Regards,<br>
    <span class="footer-name">Jhun Syriell Acain</span>
    <br><strong>Administration Officer</strong>, Human Resource Training (RTO 31678)
    <br><span class="footer-span"><strong>Phone:</strong> +61 7 3289 4444</span> <span class="footer-span"><strong>Mobile:</strong> 0437 146 670</span> <span class="footer-span"><strong>Email:</strong> <a href="mailto:jhun@hrtraining.com.au">jhun@hrtraining.com.au</a></span>
    <br><span class="footer-span"><strong>Address:</strong> PO Box 503, Samford, Qld, 4520</span> <span class="footer-span"><strong>Website:</strong> <a href="https://hrtraining.com.au/" target="_blank">www.HRtraining.com.au</a></span>
  </div>
  <div>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/1.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/2.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/3.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/4.png') }}"></span>
    <span class="footer-icons"><img src="{{ asset('assets/images/mail/5.png') }}"></span>
  </div>
  <div class="footer-notice">
    This e-mail and any attachments are for authorised use by the intended recipient only. It may contain proprietary material, confidential information and/ or be subject to legal privilege. It, therefore, should not be copied,
    <br>disclosed to,  retained or used by any other party. If you are not an intended recipient please inform the sender by reply e-mail and promptly delete this e-mail and any attachments.
  </div>
  <div class="footer-company">
    HRD Consulting Australasia Pty Ltd trading as Human Resource Training  ACN 121 890 917  ABN 68 121 890 917
  </div>
@endcomponent
