<div class="sidebar-right">
  
  <div class="content-block">
    <div class="block-header">
      LIVE MENTOR HELP
    </div>
    <div class="block-body">
      <p class="text-old-blue font-bold">Our live support is available from 10:00 to 17:00 Mon to Fri.</p>
      <p><a href="https://tawk.to/chat/575116fd9ed8cf003ee964f9/default/?$_tawk_popout=true"><img src="/assets/images/live-chat-click-here.png" class="img-fluid"></a></p>
      <p><a href="http://hrtraining.com.au/contact-your-mentor-for-help/" target="_blank">CONTACT MENTOR FOR HELP 24/7</a></p>
      <p><a href="http://hrtraining.com.au/help-hrtraining-college-improve/" target="_blank" class="text-old-red">Your Suggestions to <br>improve HRtraining college</a></p>
    </div>
  </div>

  <div class="content-block">
    <div class="block-header">
      NOTICE BOARD
    </div>
    <div class="block-body">
      <ul class="list-inline social-list">
        <li class="list-inline-item"><a href="https://hrtraining.com.au/" target="_blank"><img src="/assets/images/hrtraining-icon.png"></a></li>
        <li class="list-inline-item"><a href="https://www.facebook.com/Human-Resource-Training-753924044622355/?ref=hl" target="_blank"><img src="/assets/images/facebook.png"></a></li>
        <li class="list-inline-item"><a href="https://twitter.com/HRtrainingAUS" target="_blank"><img src="/assets/images/twitter.png"></a></li>
        <li class="list-inline-item"><a href="https://plus.google.com/+HrtrainingAustralia" target="_blank"><img src="/assets/images/googleplus.png"></a></li>
        <li class="list-inline-item"><a href="skype:humanresourcetraining?call" target="_blank"><img src="/assets/images/skype.png"></a></li>
      </ul>
    </div>
  </div>

  <div class="content-block">
    <div class="text-center pt-3 pb-3">
      <img src="/assets/images/nrt.png" class="img-fluid" width="200">
    </div>
  </div>

</div>
