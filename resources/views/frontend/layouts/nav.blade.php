<nav class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="#"><img src="/assets/images/hrtrainingcollege_logo.png" class="img-fluid"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav ml-auto">
      @guest
      <li class="nav-item active">
        <div class="nav-link">
          You are not logged in. <a href="{{ route('login') }}">(Login)</a>
        </div>
      </li>
      @endguest
    </ul>
  </div>
</nav>
