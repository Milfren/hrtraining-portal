@extends('frontend.layouts.master')

@section('title')
  Enrolment Form
@endsection

@section('header-block')
  Enrolment Form
@endsection

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Library</a></li>
      <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
  </nav>
@endsection

@section('content')
  <enrolment-form></enrolment-form>
@endsection
