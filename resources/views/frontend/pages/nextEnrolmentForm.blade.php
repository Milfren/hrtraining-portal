@extends('frontend.layouts.master')

@section('title')
  Enrolment Form
@endsection

@section('header-block')
  Enrolment Form
@endsection

@section('breadcrumb')
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Library</a></li>
      <li class="breadcrumb-item active" aria-current="page">Data</li>
    </ol>
  </nav>
@endsection

@section('content')
  <form>
    <div class="form-group row">
        <div class="col-sm-12">
          <label>1.) Enter your full name</label>
        </div>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="firstName" aria-describedby="firstNameHelp" placeholder="First Name">
          <small id="firstNameHelp" class="form-text text-muted">First Name</small>
        </div>
        <div class="col-sm-6">
          <input type="text" class="form-control" id="lastName" aria-describedby="lastNameHelp" placeholder="Last Name">
          <small id="lastNameHelp" class="form-text text-muted">Last Name</small>
        </div>
        <div class="col-sm-12">
          <p>The name above will appear on your certificate & results issued to you by Human Resource Training.</p>
        </div>
    </div>
    <div class="form-group">
      <label for="birthDate">2.) Enter your birth date</label>
      <input type="date" name="birthDate" id="birthDate" class="form-control">
    </div>
    <div class="form-group row">
      <label class="col">3.) Sex</label>
      <div class="col-sm-12">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="sexRadios" id="sexRadios1" value="Male">
          <label class="form-check-label" for="sexRadios1">
            Male
          </label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="sexRadios" id="sexRadios2" value="Female">
          <label class="form-check-label" for="sexRadios2">
            Female
          </label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="sexRadios" id="sexRadios3" value="Not stated">
          <label class="form-check-label" for="sexRadios3">
            Not stated
          </label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="sexRadios" id="sexRadios4" value="Unspecified">
          <label class="form-check-label" for="sexRadios4">
            Unspecified
          </label>
        </div>
      </div>
    </div>
    <div class="form-group row">
      <label class="col">4.) Do you have a USI? (Unique Student Identifier)</label>
      <div class="col-sm-12">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="usiRadios" id="usiRadios1" value="Yes">
          <label class="form-check-label" for="usiRadios1">
            Yes
          </label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="usiRadios" id="usiRadios2" value="No">
          <label class="form-check-label" for="usiRadios2">
            No
          </label>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="usiNumberInput">Enter your USI number:</label>
      <input type="text" class="form-control" name="usiNumberInput" id="usiNumberInput" aria-describedby="usiNumberInputHelp" placeholder="USI Number">
      <small id="usiNumberInputHelp" class="form-text text-muted">0 of 10 max characters</small>
    </div>
    <p>To create your own USI, <a href="http://hrtraining.com.au/usi/" target="_blank">Click here!</a></p>
    <div class="form-group row">
      <label class="col-sm-12">5.) What is your home residential address?</label>
      <div class="col-sm-12 mb-1">
        <input type="text" class="form-control" name="residentialAddressInput1" id="residentialAddressInput1" aria-describedby="residentialAddressInputHelp1" placeholder="Street Address">
        <small id="residentialAddressInputHelp1" class="form-text text-muted">Street Address</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="residentialAddressInput2" id="residentialAddressInput2" aria-describedby="residentialAddressInputHelp2" placeholder="City">
        <small id="residentialAddressInputHelp2" class="form-text text-muted">City</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="residentialAddressInput3" id="residentialAddressInput3" aria-describedby="residentialAddressInputHelp3" placeholder="State / Province">
        <small id="residentialAddressInputHelp3" class="form-text text-muted">State / Province</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="residentialAddressInput4" id="residentialAddressInput4" aria-describedby="residentialAddressInputHelp4" placeholder="ZIP / Postal Code">
        <small id="residentialAddressInputHelp4" class="form-text text-muted">ZIP / Postal Code</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="residentialAddressInput5" id="residentialAddressInput5" aria-describedby="residentialAddressInputHelp5" placeholder="Country">
        <small id="residentialAddressInputHelp5" class="form-text text-muted">Country</small>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-12">6.) What is your postal address?</label>
      <div class="col-sm-12 mb-3">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="" id="postalAddress1">
          <label class="form-check-label" for="postalAddress1">
            Same as above
          </label>
        </div>
      </div>
      <div class="col-sm-12 mb-1">
        <input type="text" class="form-control" name="postalAddressInput1" id="postalAddressInput1" aria-describedby="postalAddressInputHelp1" placeholder="Street Address">
        <small id="postalAddressInputHelp1" class="form-text text-muted">Street Address</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="postalAddressInput2" id="postalAddressInput2" aria-describedby="postalAddressInputHelp2" placeholder="City">
        <small id="postalAddressInputHelp2" class="form-text text-muted">City</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="postalAddressInput3" id="postalAddressInput3" aria-describedby="postalAddressInputHelp3" placeholder="State / Province">
        <small id="postalAddressInputHelp3" class="form-text text-muted">State / Province</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="postalAddressInput4" id="postalAddressInput4" aria-describedby="postalAddressInputHelp4" placeholder="ZIP / Postal Code">
        <small id="postalAddressInputHelp4" class="form-text text-muted">ZIP / Postal Code</small>
      </div>
      <div class="col-sm-6 mb-1">
        <input type="text" class="form-control" name="postalAddressInput5" id="postalAddressInput5" aria-describedby="postalAddressInputHelp5" placeholder="Country">
        <small id="postalAddressInputHelp5" class="form-text text-muted">Country</small>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-12">7.) Contact Information</label>
      <div class="col-sm-6 mb-1">
        <label for="contactInfo1">Home Number</label>
        <input type="text" class="form-control" name="contactInfo1" id="contactInfo1" placeholder="Home Number">
      </div>
      <div class="col-sm-6 mb-1">
        <label for="contactInfo2">Mobile Number</label>
        <input type="text" class="form-control" name="contactInfo2" id="contactInfo2" placeholder="Mobile Number">
      </div>
      <div class="col">
        <label for="contactInfo3">Email Address</label>
        <input type="email" class="form-control" name="contactInfo3" id="contactInfo3" placeholder="Email Address">
      </div>
    </div>

    <h2>Course Details</h2>
    <div class="form-group row">
      <label for="courseCategorySelect" class="col-sm-12">8.) I wish to enrol in the following course <span class="text-danger">(select category AND then a COURSE from the drop down box)</span></label>
      <div class="col-sm-12">
        <select id="courseCategorySelect" class="form-control">
          <option>Select a Course Category</option>
          <option>Business and Management Courses</option>
          <option>Cleaning Operations and Cleaning Management Courses</option>
          <option>Security Operations and Risk Management Courses</option>
          <option>Fatigue Management Courses</option>
          <option>Warehousing and Logistics</option>
          <option>Other</option>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label for="courseNameSelect" class="col-sm-12">Course name (select a course from the drop down box):</label>
      <div class="col-sm-12">
        <select id="courseNameSelect" class="form-control">
          <option>Select a Cleaning Course</option>
          <option>Business and Management Courses</option>
          <option>Cleaning Operations and Cleaning Management Courses</option>
          <option>Security Operations and Risk Management Courses</option>
          <option>Fatigue Management Courses</option>
          <option>Warehousing and Logistics</option>
          <option>Other</option>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-12">9.) Are you applying for RPL / National Recognition for any units?</label>
      <div class="col-sm-12">
        <p>What is RPL?</p>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="rplRadios" id="rplRadios1" value="Yes">
          <label class="form-check-label" for="rplRadios">
            Yes (HR training will contact you)
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="rplRadios2" id="rplRadios2" value="No">
          <label class="form-check-label" for="rplRadios2">
            No
          </label>
        </div>
      </div>
    </div>
    <h2>Language and Cultural Diversity</h2>
    <div class="form-group row">
      <label for="countryBornInput" class="col-sm-12">10.) In which country were you born?</label>
      <div class="col-sm-12">
        <input type="text" class="form-control" name="countryBornInput" id="countryBornInput" placeholder="Country">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-12">11.) Do you speak a language, other than English at home?</label>
      <div class="col-sm-12 mb-2">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="englishOnly" id="englishOnly" value="No">
          <label class="form-check-label" for="englishOnly">
            No - English Only
          </label>
        </div>
      </div>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="otherLanguage" id="otherLanguage" placeholder="Other">
      </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
