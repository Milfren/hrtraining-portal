require('./bootstrap');

window.Vue = require('vue');
window.toastr = require('toastr');
window._debounce = require('lodash/debounce');
window.moment = require('moment');

import store from './store';

Vue.component('front-page', require('./components/FrontPage.vue'));

Vue.component('course-category', require('./components/CourseCategory.vue'));

Vue.component('course-unit-list', require('./components/CourseUnitList.vue'));
Vue.component('course-list', require('./components/CourseList.vue'));

// Enrolment
Vue.component('enrolment-form', require('./components/EnrolmentForm.vue'));
Vue.component('enrolment-list', require('./components/EnrolmentList.vue'));
Vue.component('enrolment-entry', require('./components/admin/enrolment/entry.vue'));
Vue.component('enrolment-settings', require('./components/admin/enrolment/settings.vue'));
// Vue.component('enrolment-entry', require('./components/EnrolmentEntry.vue'));

// Admin Components
Vue.component('admin-student-list', require('./components/admin/student/list.vue'));
Vue.component('admin-student-show', require('./components/admin/student/show.vue'));

// Admin/Clients
Vue.component('admin-clients-list', require('./components/admin/clients/list.vue'));
Vue.component('admin-clients-show', require('./components/admin/clients/show.vue'));

// Student components
Vue.component('student-information', require('./components/student/Information.vue'));

// CompletionReport
Vue.component('completion-report-vue', require('./components/report/completionreport.vue'));

// Loading Spinner Component
Vue.component('spin-circle', require('vue-loading-spinner/src/components/DoubleBounce.vue'));

// Passport Components
Vue.component('passport-clients', require('./components/admin/developer/Clients.vue'));
Vue.component('passport-authorized-clients', require('./components/admin/developer/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens', require('./components/admin/developer/PersonalAccessTokens.vue'));


const app = new Vue({
    el: '#app',
    store
});
