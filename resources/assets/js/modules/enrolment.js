const enrolment = {
  namespaced: true,
  state: {
    pageLoaded: false,
    list: [],
    entry: {},
    settings: null,
    sex: [
      {id: 1, name: 'Male'},
      {id: 2, name: 'Female'},
      {id: 3, name: 'Not stated'},
      {id: 4, name: 'Unspecified'}
    ],
    english: [
      {id: 1, name: 'Very Well'},
      {id: 2, name: 'Well'},
      {id: 3, name: 'Not Well'},
      {id: 4, name: 'Not at all'}
    ],
    aboriginal: [
      {id: 1, name: 'No'},
      {id: 2, name: 'Yes, Aboriginal'},
      {id: 3, name: 'Yes, Torres Strait Islander'},
      {id: 4, name: 'Yes, other indigenous origin'}
    ],
    disabilities: [
      {id: 1, name: 'Hearing/Deaf', status: false},
      {id: 2, name: 'Physical', status: false},
      {id: 3, name: 'Intellectual', status: false},
      {id: 4, name: 'Learning', status: false},
      {id: 5, name: 'Mental Illness', status: false},
      {id: 6, name: 'Acquired Brain Impairment', status: false},
      {id: 7, name: 'Vision', status: false},
      {id: 8, name: 'Medical Condition', status: false},
      {id: 9, name: 'Other', status: false}
    ],
    schooling: [
      {id: 1, name: 'Year 12 or equivalent'},
      {id: 2, name: 'Year 11 or equivalent'},
      {id: 3, name: 'Year 10 or equivalent'},
      {id: 4, name: 'Year 9 or equivalent'},
      {id: 5, name: 'Year 8 or equivalent'},
      {id: 6, name: 'Never attended school'}
    ],
    qualifications: [
      {id: 1, name: 'Bachelor Degree or Higher Degree', status: false},
      {id: 2, name: 'Advanced Diploma or Associate Degree', status: false},
      {id: 3, name: 'Diploma (or Associate Diploma)', status: false},
      {id: 4, name: 'Certificate IV (or Advanced Certificate/Technician)', status: false},
      {id: 5, name: 'Certificate III (or Trade Certificate)', status: false},
      {id: 6, name: 'Certificate II', status: false},
      {id: 7, name: 'Certificate I', status: false},
      {id: 8, name: 'Certificates other than above', status: false}
    ],
    employment_status: [
      {id: 1, name: 'Full-time employee'},
      {id: 2, name: 'Part-time employee'},
      {id: 3, name: 'Self-employed – not employing others'},
      {id: 4, name: 'Employer'},
      {id: 5, name: 'Employed – unpaid worker in a family business'},
      {id: 6, name: 'Unemployed – seeking full-time work'},
      {id: 7, name: 'Unemployed – seeking part-time work'},
      {id: 8, name: 'Not employed – not seeking employment'}
    ],
    study_reason: [
      {id: 1, name: 'To get a job'},
      {id: 2, name: 'To develop my existing business'},
      {id: 3, name: 'To start my own business'},
      {id: 4, name: 'To try a different career'},
      {id: 5, name: 'To get a better job or promotion'},
      {id: 6, name: 'It was a requirement of my job'},
      {id: 7, name: 'I wanted extra skills for my job'},
      {id: 8, name: 'To get into another course of study'},
      {id: 9, name: 'For personal interest or self-development'},
      {id: 10, name: 'Other reasons'}
    ],
    identification: [
      {id: 1, name: 'Driver Licence (Front and Back)', status: false},
      {id: 2, name: 'Passport', status: false},
      {id: 3, name: 'Other', status: false}
    ],
    documents: [
      {id: 1, name: 'ID – drivers licence (Front and Back) / Passport', status: false},
      {id: 2, name: 'Certificates/ Statements of Attainment', status: false},
      {id: 3, name: 'Head & shoulders Photo', status: false},
      {id: 4, name: 'First Aid Certificate (if applicable)', status: false},
      {id: 5, name: 'Security Licence (if applicable)', status: false}
    ]
  },
  mutations: {
    setEntry(state, payload) {
      state.entry = payload;
    },
    getEntries(state, payload) {
      axios.get('/api/admin/enrolment')
      .then(res => {
        state.list = res.data;
      })
      .catch(err => {
        console.log(err);
      });
    },
    getSettings(state, payload) {
      axios.get('/api/admin/enrolment-settings/1')
      .then(res => {
        console.log(res.data);
        state.settings = res.data;
        state.pageLoaded = true;
      })
      .catch(err => {
        console.log(err);
      });
    },
    async addSettings(state, payload) {
      return await axios({
        method: 'post',
        url: `/api/admin/enrolment-settings/`,
        data: {
          form_id: 1,
          type: payload.type,
          email: payload.email
        }
      }).then(res => {
        state.settings = res.data;
      }).catch(err => {
        console.log(err);
      });
    },
    refreshSettings(state, payload) {
      state.settings = payload;
    },
    deleteEntry(state, payload) {
      axios({
        method: 'delete',
        url: `/api/admin/enrolment/${payload}`
      }).then(response => {
        state.list = response.data;
        toastr.success('Enrolment Deleted!');
      }).catch(error => {
        console.log(error);
      });
    }
  },
  actions: {
    refreshSettings({ commit }, payload) {
      return new Promise((resolve, reject) => {
        commit('refreshSettings', payload);
        resolve();
      })
    },
    addSettings({ commit }, payload) {
      return commit('addSettings', payload);
    }
  }
}

export default enrolment;
