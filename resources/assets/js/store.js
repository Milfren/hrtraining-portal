import Vue from 'vue';
import Vuex from 'vuex';

import countries from './modules/countries';
import enrolment from './modules/enrolment';

Vue.use(Vuex);

const client = {
  namespaced: true,
  state: {
    client: {}
  },
  mutations: {
    setClient(state, client) {
      state.client = client;
    },
    addStudent(state, payload) {
      state.client.students = payload;
    },
    refreshCategory(state, payload) {
      state.client.categories = payload;
    },
    refreshCategoryStudent(state, payload) {
      state.client.categories[payload.index].students = payload.res;
    },
    refreshFiles(state, payload) {
      state.client.files = payload;
    },
  },
}

const store = new Vuex.Store({
  state: {
    lms_token: 'bddfb9cfbf90cb85e6862e4eecf23b65',
    lms_domain: `https://cors-anywhere.herokuapp.com/https://hrtrainingcollege.com/webservice/rest/server.php?moodlewsrestformat=json&wstoken=`
  },
  modules: {
    client: client,
    countries: countries,
    enrolment: enrolment
  }
});

export default store;
